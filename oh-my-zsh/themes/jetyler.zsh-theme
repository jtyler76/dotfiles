## Colour Codes
##   To see available colours and their associated codes, type 'spectrum_ls' in your terminal
# Bright Yellow: 011
# Bright Cyan: 014

## Unicode Compatibilty
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

## Single Line Prompt when not in Git repo
function myprompt() {
    if ! { git rev-parse --git-dir > /dev/null 2>&1 }; then
        echo -n "%{%B%F{014}%}%(4~|.../%2~|%~)%{%f%b%} "
        echo -n "$(git_prompt_info)"
    fi
    echo -n "%{%B%(?.%F{green}.%F{red})%}->%{%f%b%} "
}

## First line of prompt when in Git repo
function precmd() {
    if { git rev-parse --git-dir > /dev/null 2>&1 }; then
        local cwd="%{%B%F{014}%}%(4~|.../%2~|%~)%{%f%b%} "
        local gitbranch="$(git_prompt_info)"
        print -Pr "${qnxenv}${cwd}${gitbranch}"
    fi
}

## Git Configuration
ZSH_THEME_GIT_PROMPT_PREFIX="%{%B%F{blue}%}(%{%F{red}%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{%f%b%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{%B%F{blue}%}) %{%F{011}%}✗"
ZSH_THEME_GIT_PROMPT_CLEAN="%{%B%F{blue}%})"
ZSH_THEME_GIT_SHOW_UPSTREAM=1

PROMPT='$(myprompt)'
