#!/usr/bin/env bash

LOG_POLYBAR_MAIN = "/tmp/polybar_main.log"

## Kill existing polybar instances
polybar-msg cmd quit

## Launch the main bar
echo "---" | tee -a ${LOG_POLYBAR_MAIN}
polybar main 2>&1 | tee -a ${LOG_POLYBAR_MAIN} & disown

echo "Bars launched..."
