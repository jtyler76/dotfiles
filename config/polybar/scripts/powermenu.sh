#!/bin/bash

RESULT="$(rofi                   \
          -no-lazy-grab          \
          -sep '|'               \
          -dmenu                 \
          -i                     \
          -p 'pmenu'           \
          -hide-scrollbar true   \
          -bw 0                  \
          -lines 4               \
          -line-padding          \
          -padding 20            \
          -width 15              \
          -xoffset -27           \
          -yoffset 60            \
          -location 3            \
          -columns 1             \
          -color-enabled true    \
          -theme ~/.config/rofi/themes/rounded-blue.rasi   \
          -theme-str '#window { location: north east; }'   \
          -theme-str '#window { anchor: north east; }'     \
          -theme-str '#window { width: 200; }'             \
          -theme-str '#window { y-offset: 40; }'           \
          -theme-str '#window { x-offset: -5; }'           \
          -theme-str '#listview { lines: 6; }'             \
          -theme-str '#inputbar { enabled: false; }'       \
<<< ' Lock| Logout| Suspend| Hibernate| Reboot| Shutdown')"

case "${RESULT}" in
    *Lock) i3lock-fancy ;;
    *Logout) i3-msg exit;;
    *Suspend) systemctl suspend-then-hibernate ;;
    *Hibernate) systemctl hibernate ;;
    *Reboot) systemctl reboot ;;
    *Shutdown) systemctl poweroff ;;
esac
