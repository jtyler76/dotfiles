#!/bin/bash

BLUETOOTH_ENABLED="false"
DEVICES_CONNECTED="false"

## Check if bluetooth is enabled
if bluetoothctl devices | grep "No default controller available" > /dev/null; then
    echo "%{F#707880}%{F-}"
    exit
else
    echo -n "%{F#64b2f1}%{F-}"
fi

## Determine if ANY device is connected
while IFS= read -r uuid; do
    CONNECTED=$(bluetoothctl info ${uuid} | grep Connected | awk '{print $2}')
    if [ -z ${CONNECTED} ]; then
        exit
    fi
    if [ ${CONNECTED} = "yes" ]; then
        DEVICES_CONNECTED=true
    fi
done <<< $(bluetoothctl devices | cut -d' ' -f2)

if [ ${DEVICES_CONNECTED} = "true" ]; then
    echo " %{F#707880}%{F-}"
fi
