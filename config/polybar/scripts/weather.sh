#!/bin/bash

## --- Request Settings
APIKEY=`cat ${HOME}/.owm-key`
CITY="Ottawa"
COUNTRY="CA"
LANG="en"
UNITS="metric"

## --- Colours
COLOUR_CLOUD="#AFAFAF"
COLOUR_THUNDER="#d3b987"
COLOUR_LIGHT_RAIN="#73cef4"
COLOUR_HEAVY_RAIN="#b3deef"
COLOUR_SNOW="#FFFFFF"
COLOUR_FOG="#AFAFAF"
COLOUR_TORNADO="#d3b987"
COLOUR_SUN="#ffc24b"
COLOUR_MOON="#FFFFFF"
COLOUR_ERR="#f43753"
COLOUR_WIND="#73cef4"
COLOUR_COLD="#b3deef"
COLOUR_HOT="#f43753"
COLOUR_NORMAL_TEMP="#FFFFFF"
COLOUR_VOLCANIC_ASH="#FFA500"

## --- Icons
ICON_THUNDER=""
ICON_DRIZZLE_LIGHT_DAY=""
ICON_DRIZZLE_LIGHT_NIGHT=""
ICON_DRIZZLE_MODERATE=""
ICON_DRIZZLE_HEAVY=""
ICON_RAIN_LIGHT_DAY=""
ICON_RAIN_LIGHT_NIGHT=""
ICON_RAIN_MODERATE=""
ICON_RAIN_HEAVY=""
ICON_RAIN_FREEZING=""
ICON_SNOW=""
ICON_MIST=""
ICON_SMOKE=""
ICON_HAZE=""
ICON_DUST=""
ICON_FOG=""
ICON_SAND=""
ICON_VOLCANIC_ASH=""
ICON_SQUALLS=""
ICON_TORNADO=""
ICON_CLEAR_SKIES_DAY=""
ICON_CLEAR_SKIES_NIGHT=""
ICON_PARTLY_CLOUDY_DAY=""
ICON_PARTLY_CLOUDY_NIGHT=""
ICON_OVERCAST=""

## --- Build Request URL
URL_BASE="api.openweathermap.org/data/2.5/weather?"
URL_APPID="appid=${APIKEY}"
URL_UNITS="&units=${UNITS}"
URL_LANG="&lang=${LANG}"
URL_LOCATION="&q=${CITY},${COUNTRY}"
URL="${URL_BASE}${URL_APPID}${URL_UNITS}${URL_LANG}${URL_LOCATION}"

## --- Request Weather Data
RAW_WEATHER_DATA=`curl -s ${URL}`
if [ ${?} -ne 0 ]; then
    echo "unavailable"
fi

## --- Print Raw Data with Debug Option
if [ "${1}" = "-d" ]; then
    echo ${RAW_WEATHER_DATA}
    echo ""
fi

## --- Parse Weather Data
TEMPERATURE=`echo ${RAW_WEATHER_DATA} | jq .main.temp | sed 's/\.[0-9]*//g'`
WEATHER_MAIN=`echo ${RAW_WEATHER_DATA} | jq .weather[0].main`
WEATHER_DESC=`echo ${RAW_WEATHER_DATA} | jq .weather[0].description`
WEATHER_ID=`echo ${RAW_WEATHER_DATA} | jq .weather[0].id`
SUNRISE=`echo ${RAW_WEATHER_DATA} | jq .sys.sunrise`
SUNSET=`echo ${RAW_WEATHER_DATA} | jq .sys.sunset`
TIME=`date +%s`

## --- Create Weather Indicator
## ---   Weather codes: https://openweathermap.org/weather-conditions
# Thunder
if [ ${WEATHER_ID} -ge 200 -a ${WEATHER_ID} -le 232 ]; then
    WEATHER="%{F${COLOUR_THUNDER}}${ICON_THUNDER}%{F-}"
fi
# Dizzle, Light
if [ ${WEATHER_ID} -eq 300 -o ${WEATHER_ID} -eq 310 ]; then
    if [ ${TIME} -ge ${SUNRISE} -a ${TIME} -le ${SUNSET} ]; then
        WEATHER="%{F${COLOUR_LIGHT_RAIN}}${ICON_DRIZZLE_LIGHT_DAY}%{F-}"
    else
        WEATHER="%{F${COLOUR_LIGHT_RAIN}}${ICON_DRIZZLE_LIGHT_NIGHT}%{F-}"
    fi
fi
# Drizzle, Moderate
if [ ${WEATHER_ID} -eq 301 -o \
     ${WEATHER_ID} -eq 311 -o \
     ${WEATHER_ID} -eq 313 -o \
     ${WEATHER_ID} -eq 321 ]; then
    WEATHER="%{F${COLOUR_LIGHT_RAIN}}${ICON_DRIZZLE_MODERATE}%{F-}"
fi
# Drizzle, Heavy
if [ ${WEATHER_ID} -eq 302 -o \
     ${WEATHER_ID} -eq 312 -o \
     ${WEATHER_ID} -eq 314 ]; then
    WEATHER="%{F${COLOUR_HEAVY_RAIN}}${ICON_DRIZZLE_HEAVY}%{F-}"
fi
# Rain, Light
if [ ${WEATHER_ID} -eq 500 -o ${WEATHER_ID} -eq 512 ]; then
    if [ ${TIME} -ge ${SUNRISE} -a ${TIME} -le ${SUNSET} ]; then
        WEATHER="%{F${COLOUR_LIGHT_RAIN}}${ICON_RAIN_LIGHT_DAY}%{F-}"
    else
        WEATHER="%{F${COLOUR_LIGHT_RAIN}}${ICON_RAIN_LIGHT_NIGHT}%{F-}"
    fi
fi
# Rain, Moderate
if [ ${WEATHER_ID} -eq 502 -o \
     ${WEATHER_ID} -eq 520 -o \
     ${WEATHER_ID} -eq 521 ]; then
    WEATHER="%{F${COLOUR_LIGHT_RAIN}}${ICON_RAIN_MODERATE}%{F-}"
fi
# Rain, Heavy
if [ ${WEATHER_ID} -eq 503 -o \
     ${WEATHER_ID} -eq 504 -o \
     ${WEATHER_ID} -eq 522 -o \
     ${WEATHER_ID} -eq 531 ]; then
    WEATHER="%{F${COLOUR_HEAVY_RAIN}}${ICON_RAIN_HEAVY}%{F-}"
fi
# Rain, Freezing
if [ ${WEATHER_ID} -eq 511 ]; then
    WEATHER="%{F${COLOUR_COLD}}${ICON_RAIN_FREEZING}%{F-}"
fi
# Snowing
if [ ${WEATHER_ID} -ge 600 -a ${WEATHER_ID} -le 622 ]; then
    WEATHER="%{F${COLOUR_COLD}}${ICON_SNOW}%{F-}"
fi
# Mist
if [ ${WEATHER_ID} -eq 701 ]; then
    WEATHER="%{F${COLOUR_HEAVY_RAIN}}${ICON_MIST}%{F-}"
fi
# Smoke
if [ ${WEATHER_ID} -eq 711 ]; then
    WEATHER="%{F${COLOUR_FOG}}${ICON_SMOKE}%{F-}"
fi
# Haze
if [ ${WEATHER_ID} -eq 721 ]; then
    WEATHER="%{F${COLOUR_FOG}}${ICON_HAZE}%{F-}"
fi
# Dust
if [ ${WEATHER_ID} -eq 731 -o ${WEATHER_ID} -eq 761 ]; then
    WEATHER="%{F${COLOUR_FOG}}${ICON_DUST}%{F-}"
fi
# Fog
if [ ${WEATHER_ID} -eq 741 ]; then
    WEATHER="%{F${COLOUR_FOG}}${ICON_FOG}%{F-}"
fi
# Sand
if [ ${WEATHER_ID} -eq 751 ]; then
    WEATHER="%{F${COLOUR_FOG}}${ICON_SAND}%{F-}"
fi
# Volcanic Ash
if [ ${WEATHER_ID} -eq 762 ]; then
    WEATHER="%{F${COLOUR_VOLCANIC_ASH}}${ICON_VOLCANIC_ASH}%{F-}"
fi
# Squalls
if [ ${WEATHER_ID} -eq 771 ]; then
    WEATHER="%{F${COLOUR_HEAVY_RAIN}}${ICON_SQUALLS}%{F-}"
fi
# Tornado
if [ ${WEATHER_ID} -eq 781 ]; then
    WEATHER="%{F${COLOUR_TORNADO}}${ICON_TORNADO}%{F-}"
fi
# Clear Skies
if [ ${WEATHER_ID} -eq 800 ]; then
    if [ ${TIME} -ge ${SUNRISE} -a ${TIME} -le ${SUNSET} ]; then
        WEATHER="%{F${COLOUR_SUN}}${ICON_CLEAR_SKIES_DAY}%{F-}"
    else
        WEATHER="%{F${COLOUR_MOON}}${ICON_CLEAR_SKIES_NIGHT}%{F-}"
    fi
fi
# Clouds, Partly Cloudy
if [ ${WEATHER_ID} -eq 801 -o ${WEATHER_ID} -eq 802 ]; then
    if [ ${TIME} -ge ${SUNRISE} -a ${TIME} -le ${SUNSET} ]; then
        WEATHER="%{F${COLOUR_CLOUD}}${ICON_PARTLY_CLOUDY_DAY}%{F-}"
    else
        WEATHER="%{F${COLOUR_CLOUD}}${ICON_PARTLY_CLOUDY_NIGHT}%{F-}"
    fi
fi
# Clouds, Overcast
if [ ${WEATHER_ID} -eq 803 -o ${WEATHER_ID} -eq 804 ]; then
    WEATHER="%{F${COLOUR_CLOUD}}${ICON_OVERCAST}%{F-}"
fi

## --- Send Formatted Weather String to Polybar
echo -n "${WEATHER} ${TEMPERATURE}°C"
