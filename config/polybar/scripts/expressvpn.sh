#!/bin/bash

if ! command -v expressvpn > /dev/null 2>&1; then
    exit
fi

STATUS=$(expressvpn status | grep Connected)

if [[ -z ${STATUS} ]]; then
    ## Disconnected
    echo -n "%{F#C24C57}%{F-}"
else
    ## Connected
    echo -n "%{F#6ebe73}%{F-} "
    expressvpn status | grep --color=never Connected | sed 's/.* - //g'
fi
