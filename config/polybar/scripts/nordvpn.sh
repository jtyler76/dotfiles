#!/bin/bash

STATUS=$(nordvpn status | grep Status | awk '{print $NF}')

if [[ ${STATUS} == "Connected" ]]; then
    echo -n "%{F#6ebe73}%{F-} "
    nordvpn status | grep City | awk '{print $NF}'
else
    echo -n "%{F#C24C57}%{F-}"
fi
