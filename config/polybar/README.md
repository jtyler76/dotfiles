# Dependencies

| Dependency                | Source                     | Package                             |
|---------------------------|----------------------------|-------------------------------------|
| Polybar                   | Arch Official Repositories | polybar                             |
| Noto Sans Mono Font       | Arch Official Repositories | noto-fonts                          |
| Noto Sans Mono Nerd Fonts | Arch Official Repositories | nerd-fonts-noto-sans-mono           |
| Font Awesome 6 Fonts      | Arch User Repository       | otf-font-awesome & ttf-font-awesome |
| Fira Code Font            | Arch Official Repositories | ttf-fira-code                       |
| Fira Code Nerd Font       | Arch User Repository       | otf-nerd-fonts-fira-mono            |
| networkmanager_dmenu      | Arch Official Repositories | networkmanager-dmenu-git            |
| nordvpn                   | Arch User Repository       | nordvpn-bin                         |
| blueberry                 | Arch Official Repositories | blueberry                           |
| bluetoothctl              | Arch Official Repositories | bluez-utils                         |
| rofi                      | Arch Official Repositories | rofi                                |
| betterlockscreen          | Arch Official Repositories | betterlockscreen                    |
