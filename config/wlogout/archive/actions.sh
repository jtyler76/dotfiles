#!/bin/bash

if [[ "$1" == '--shutdown' ]]; then
	systemctl poweroff

elif [[ "$1" == '--reboot' ]]; then
	systemctl reboot

elif [[ "$1" == '--hibernate' ]]; then
    sleep 1
    ~/.config/hypr/scripts/lockscreen.sh &
    sleep 1
    systemctl hibernate

elif [[ "$1" == '--lock' ]]; then
	~/.config/hypr/scripts/lockscreen.sh

elif [[ "$1" == '--suspend' ]]; then
    sleep 1
    mpc -q pause
    pulsemixer --mute
    ~/.config/hypr/scripts/lockscreen.sh &
    sleep 1
    systemctl suspend-then-hibernate

elif [[ "$1" == '--logout' ]]; then
	hyprctl dispatch exit 0
fi
