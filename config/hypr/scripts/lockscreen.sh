#!/bin/bash

if [[ ! $(pidof swaylock) ]]; then
    swaylock                                       \
        --clock                                    \
        --screenshots                              \
        --indicator                                \
        --indicator-radius     200                 \
        --indicator-thickness  6                   \
        --effect-blur          7x5                 \
        --effect-vignette      1.0:0.5             \
        --fade-in              0.4                 \
        --text-color           ffffffff            \
        --key-hl-color         64b2f1ff            \
        --separator-color      ffffff00            \
        --line-color           282a2ef6            \
        --inside-color         282a2ef6            \
        --ring-color           282a2eff            \
        --timestr              "%I:%M %p"          \
        --datestr              "%a, %B %d"         \
        --inside-ver-color     282a2ef6            \
        --line-ver-color       282a2ef6            \
        --ring-ver-color       282a2ef6            \
        --text-ver-color       282a2e00            \
        --inside-wrong-color   282a2ef6            \
        --line-wrong-color     282a2ef6            \
        --ring-wrong-color     f56545ff            \
        --text-wrong-color     282a2e00            \
        --inside-clear-color   282a2ef6            \
        --line-clear-color     282a2ef6            \
        --ring-clear-color     f2cd48ff            \
        --text-clear-color     282a2e00
fi
