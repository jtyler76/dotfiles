#!/bin/bash

## Displays
DP_1="DP-1"
EDP_1="eDP-1"
HDMI="HDMI-A-2"

## The following variable will store the previously assigned display.
PREV_DISPLAY=""

## The frequency at which we'll check display connections.
SLEEP_DURATION=2

## Wallpapers for various display resolutions.
WALLPAPER_LAPTOP="${HOME}/.wallpapers/1920x1080_ocean.jpg"
WALLPAPER_ULTRAWIDE="${HOME}/.wallpapers/3440x1440_mountains.jpg"

while true; do
    sleep ${SLEEP_DURATION}

    if hyprctl monitors | grep "Monitor ${DP_1}"; then

        if [ "${PREV_DISPLAY}" != "${DP_1}" ]; then
            PREV_DISPLAY=${DP_1}

            ## Enable Ultrawide Display
            hyprctl keyword monitor ${DP_1},highres,0x0,1

            ## Disable Laptop Display
            hyprctl keyword monitor ${EDP_1},disable
        fi

    elif hyprctl monitors | grep "Monitor ${HDMI}"; then

        if [ "${PREV_DISPLAY}" != "${HDMI}" ]; then
            PREV_DISPLAY=${HDMI}

            ## Enable Laptop Display
            hyprctl keyword monitor ${HDMI},highres,0x0,1

            ## Disable Ultrawide Display
            hyprctl keyword monitor ${EDP_1},disable
        fi

    elif hyprctl monitors | grep "Monitor ${EDP_1}"; then

        if [ "${PREV_DISPLAY}" != "${EDP_1}" ]; then
            PREV_DISPLAY=${EDP_1}

            ## Enable Laptop Display
            hyprctl keyword monitor ${EDP_1},highres,0x0,1

            ## Disable Ultrawide Display
            hyprctl keyword monitor ${DP_1},disable
        fi
   fi
done
