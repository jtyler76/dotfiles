#!/bin/bash

if [[ ! $(pidof waybar) ]]; then
    waybar                                              \
        --bar        main-bar                           \
        --log-level  error                              \
        --config     ${HOME}/.config/waybar/config      \
        --style      ${HOME}/.config/waybar/style.css
fi
