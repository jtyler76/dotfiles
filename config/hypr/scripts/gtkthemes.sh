#!/bin/bash

## Set GTK Themes, Icons, Cursor and Fonts

THEME='Nordic'
ICONS='Flat-Remix-Blue-Dark'
CURSOR='Nordzy-cursors'
FONT='Noto Sans 11'

SCHEMA='gsettings set org.gnome.desktop.interface'

apply_themes() {
	${SCHEMA} gtk-theme "$THEME"
	${SCHEMA} icon-theme "$ICONS"
	${SCHEMA} cursor-theme "$CURSOR"
	${SCHEMA} font-name "$FONT"
}

apply_themes
