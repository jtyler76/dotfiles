#!/bin/bash

if [[ ! $(pidof mako) ]]; then
    make --config ~/.config/mako/config
fi
