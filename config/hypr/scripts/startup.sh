#!/bin/bash

SCRIPT_DIR=${HOME}/.config/hypr/scripts

## Set the Desktop Background
hyprpaper &

## Set the Status Bar
${SCRIPT_DIR}/statusbar.sh &

## Set up the notification service
${SCRIPT_DIR}/notifications.sh &

## Set the GTK themes
${SCRIPT_DIR}/gtkthemes.sh &

## Start the Tray Applications
nm-applet &
udiskie --tray &
blueberry-tray &
flameshot &
