#!/bin/bash

swayidle -w                                                  \
    timeout 300  '~/.config/hypr/scripts/lockscreen.sh &'    \
    timeout 600  'hyprctl dispatch dpms off'                 \
    resume       'hyprctl dispatch dpms on'                  \
    before-sleep '~/.config/hypr/scripts/lockscreen.sh &'
