#!/bin/bash

CONNECTION=$(expressvpn status | grep -E 'Connected to|Not connected')

if [[ "${CONNECTION}" == "Not connected" ]]; then
    echo "-"
else
    STATUS=$(echo ${CONNECTION} | sed 's/Connected to //g' | ansifilter)
    echo ${STATUS}
fi
