#!/bin/bash

## Sometimes xrandr adds an extra "-1" to each of the devices. It's not clear why, so we need to
## handle both cases. All displays that we're looking to connect to are listed here.
DP_1="DP-1"
DP_1_1="DP-1-1"
EDP_1="eDP-1"
EDP_1_1="eDP-1-1"

## The following variable will store the previously assigned display.
PREV_DISPLAY=""

## The frequency at which we'll check display connections.
SLEEP_DURATION=3

## Wallpapers for various display resolutions.
WALLPAPER_LAPTOP="${HOME}/Pictures/wallpapers/mountains_1920x1080.jpg"
WALLPAPER_ULTRAWIDE="${HOME}/.wallpapers/3440x1440_mountains.jpg"

while true; do
    sleep ${SLEEP_DURATION}

    if xrandr | grep "\bDP-1 connected"; then

        if [ "${PREV_DISPLAY}" != "${DP_1}" ]; then
            PREV_DISPLAY=${DP_1}

            ## Enable the external monitor display
            xrandr --output eDP-1  --off    \
                   --output DP-1   --auto   \
                   --output HDMI-1 --off    \
                   --output DP-2   --off    \
                   --output HDMI-2 --off

            ## Set the 3440x1440 wallpaper
            feh --bg-tile --no-fehbg ${WALLPAPER_ULTRAWIDE}
        fi

    elif xrandr | grep "\bDP-1-1 connected"; then

        if [ "${PREV_DISPLAY}" != "${DP_1_1}" ]; then
            PREV_DISPLAY=${DP_1_1}

            ## Enable the external monitor display
            xrandr --output eDP-1-1  --off    \
                   --output DP-1-1   --auto   \
                   --output HDMI-1-1 --off    \
                   --output DP-1-2   --off    \
                   --output HDMI-1-2 --off

            ## Set the 3440x1440 wallpaper
            feh --bg-tile --no-fehbg ${WALLPAPER_ULTRAWIDE}
        fi

    elif xrandr | grep "\beDP-1 connected"; then

        if [ "${PREV_DISPLAY}" != "${EDP_1}" ]; then
            PREV_DISPLAY=${EDP_1}

            ## Enable the laptop display with default settings
            xrandr --output eDP-1  --auto   \
                   --output DP-1   --off    \
                   --output HDMI-1 --off    \
                   --output DP-2   --off    \
                   --output HDMI-2 --off

            ## Set the 1920x1080 wallpaper
            feh --bg-tile --no-fehbg ${WALLPAPER_LAPTOP}
        fi

    elif xrandr | grep "\beDP-1-1 connected"; then

        if [ "${PREV_DISPLAY}" != "${EDP_1_1}" ]; then
            PREV_DISPLAY=${EDP_1_1}

            ## Enable the external monitor display
            xrandr --output eDP-1-1  --auto   \
                   --output DP-1-1   --off    \
                   --output HDMI-1-1 --off    \
                   --output DP-1-2   --off    \
                   --output HDMI-1-2 --off

            ## Set the 3440x1440 wallpaper
            feh --bg-tile --no-fehbg ${WALLPAPER_LAPTOP}
        fi
    fi
done
