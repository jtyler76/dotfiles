#!/bin/bash

while true; do
    BATTERY_STATUS=$(cat /sys/class/power_supply/BAT0/status)
    BATTERY_PERCENTAGE=$(cat /sys/class/power_supply/BAT0/capacity)

    if [ ${BATTERY_STATUS} = "Discharging" ]; then
        if [ ${BATTERY_PERCENTAGE} -le 15 ]; then
            notify-send "Battery Low" "Connect Charger"
        fi
    fi

    sleep 150  ## every 2.5 minutes
done
