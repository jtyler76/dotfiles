# Dependencies

| Dependency                | Source                     | Package                       |
|---------------------------|----------------------------|-------------------------------|
| i3-gaps                   | Arch Official Repositories | i3-gaps                       |
| notify-send               | Arch Official Repositories | libnotify                     |
| pactl/paplay              | Arch Official Repositories | libpulse                      |
| lux                       | AUR                        | lux                           |
| kitty                     | Arch Official Repositories | kitty                         |
| flameshot                 | Arch Official Repositories | flameshot                     |
| rofi                      | Arch Official Repositories | rofi                          |
| polybar                   | Arch Official Repositories | polybar                       |
| networkmanager_dmenu      | Arch Official Repositories | networkmanager-dmenu-git      |
| deadd-notification-center | Arch Official Repositories | deadd-notification-center-bin |
| xss-lock                  | Arch Official Repositories | xss-lock                      |
| betterlockscreen          | Arch Official Repositories | betterlockscreen              |
| feh                       | Arch Official Repositories | feh                           |
| nm-applet                 | Arch Official Repositories | network-manager-applet        |
| picom                     | Arch Official Repositories | picom                         |
