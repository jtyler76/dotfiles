## My Config
set-option -g default-shell /bin/zsh
set -g default-terminal 'screen-256color'
set -g mouse on

## Select, Copy, Paste
##   Inspiration: https://www.rockyourcode.com/copy-and-paste-in-tmux/
##   Key Bindings:
##     CTRL-B + [  ::  Enters Selection/Scrolling Mode
##     v           ::  Begin Selecting
##     y           ::  Copy to System Clipboard
##     vim-keys    ::  Navigation
setw -g mode-keys vi
set-option -s set-clipboard off
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -se c -i' ## X11 Mapping
if-shell -b '[ "$WAYLAND_DISPLAY" ]' {
    unbind -T copy-mode-vi y                                                ## Override
    bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'wl-copy'  ## Wayland Mapping
}

## Plugins
set -g @plugin 'tmux-plugins/tpm'              ## -- Plugin Manager
set -g @plugin 'tmux-plugins/tmux-sensible'    ## -- Plugin Manager

set -g @plugin 'catppuccin/tmux'
set -g @catppuccin_window_tabs_enabled on

## Initialize Plugin Manager
run '~/.config/tmux/plugins/tpm/tpm'
