# Neovim Configuration

## Bug Tracking

There is a bug in Neovide that makes it incompatible with the folke/noice.nvim plugin. Because of this, I'm not using Neovide at all until it's fixed.
- folke/noice.nvim tracking:    https://github.com/folke/noice.nvim/issues/17
- neovim/neovim tracking:       https://github.com/neovim/neovim/issues/22344
- neovide/neovide tracking:     https://github.com/neovide/neovide/issues/1751
- folke/zen-mode.nvim tracking: https://github.com/folke/zen-mode.nvim/issues/69
