-- Required to be set for some plugins
vim.opt.termguicolors = true

-- Change Font Size
function G_change_font_size(increment)
    if vim.g.neovide then
        local current_guifont = vim.fn.eval("&guifont")
        local name,size = current_guifont:match("([^:]+):h(%d+)")
        local new_size = tonumber(size) + increment
        local new_guifont = string.format("%s:h%d", name, new_size)
        vim.cmd("set guifont=" .. vim.fn.fnameescape(new_guifont))
    else
        if increment > 0 then
            vim.fn.system("kitty @ set-font-size -- +" .. increment)
        else
            vim.fn.system("kitty @ set-font-size -- -" .. (increment * -1))
        end
    end
end

-- Plugins
require("lazy.plugins")

-- General Settings
require("general.options")
require("general.autocommands")
require("general.diagnostics")
require("general.keymaps")
require("general.gui")

-- Language Specific Settings
require("lang.c")
require("lang.markdown")

-- System Overrides (load only if it exists)
pcall(function()
    require("general.system_overrides")
end)
