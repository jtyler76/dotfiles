-- Make sure .ci files are treated as .c files
vim.cmd([[autocmd BufNewFile,BufRead *.ci set filetype=c]])

