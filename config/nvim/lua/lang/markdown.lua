vim.cmd('augroup MarkdownSettings')
vim.cmd('autocmd!')

vim.cmd('autocmd FileType markdown setlocal wrap')
vim.cmd('autocmd FileType markdown setlocal colorcolumn=""')

vim.cmd('augroup END')
