-- Window Management
vim.keymap.set("n", "<leader>wv", "<CMD>vsplit<CR>")               -- Create a vertical split
vim.keymap.set("n", "<leader>ws", "<CMD>split<CR>")                -- Create a horizontal split
vim.keymap.set("n", "<leader>ww", "<CMD>w<CR>")                    -- Save the buffer in the current window
vim.keymap.set("n", "<leader>wq", "<C-w>q")                        -- Close the current window
vim.keymap.set("n", "<leader>wh", "<C-w>h")                        -- Move to window on the left
vim.keymap.set("n", "<leader>wj", "<C-w>j")                        -- Move to window below
vim.keymap.set("n", "<leader>wk", "<C-w>k")                        -- Move to window above
vim.keymap.set("n", "<leader>wl", "<C-w>l")                        -- Move to window on the right
vim.keymap.set("n", "<C-Up>",     "<CMD>resize +2<CR>")            -- Increase vertical size of current window
vim.keymap.set("n", "<C-Down>",   "<CMD>resize -2<CR>")            -- Decrease vertical size of current window
vim.keymap.set("n", "<C-Left>",   "<CMD>vertical resize -2<CR>")   -- Decrease horizontal size of current window
vim.keymap.set("n", "<C-Right>",  "<CMD>vertical resize +2<CR>")   -- Increase horizontal size of current window

-- Buffer Management
vim.keymap.set("n", "<S-l>",      "<CMD>bnext<CR>")       -- Switch to the next buffer in the current window
vim.keymap.set("n", "<S-h>",      "<CMD>bprevious<CR>")   -- Switch to the previous buffer in the current window
vim.keymap.set("n", "<leader>bq", "<CMD>bd<CR>")          -- Close the current buffer

-- Tab Management
vim.keymap.set("n", "<leader>tl", "<CMD>tabnext<CR>")       -- Switch to the next tab
vim.keymap.set("n", "<leader>th", "<CMD>tabprevious<CR>")   -- Switch to the previous tab
vim.keymap.set("n", "<leader>tn", "<CMD>tabnew<CR>")        -- Create a new tab
vim.keymap.set("n", "<leader>tq", "<CMD>tabclose<CR>")      -- Close the current tab

-- Copy/Paste
vim.keymap.set("v", "p", "\"_dP")          -- Persistant paste (don't replace buffer contents on paste)
vim.keymap.set("n", "<leader>y", "\"+y")   -- Yank to system clipboard from normal mode
vim.keymap.set("v", "<leader>y", "\"+y")   -- Yank to system clipboard from visual mode
vim.keymap.set("n", "<leader>p", "\"+p")   -- Paste to system clipboard from normal mode
vim.keymap.set("v", "<leader>p", "\"+p")   -- Paste to system clipboard

-- Indentation
vim.keymap.set("v", "<", "<gv")   -- Un-indent a selected block of code
vim.keymap.set("v", ">", ">gv")   -- Indent a selected block of code

-- Text Movement - Normal Mode
vim.keymap.set("n", "<A-j>", "<CMD>m .+1<CR>==")   -- Move current line down
vim.keymap.set("n", "<A-k>", "<CMD>m .-2<CR>==")   -- Move current line up

-- File Tree
vim.keymap.set("n", "<leader>e", "<CMD>Neotree source=filesystem position=left toggle<CR>")          -- Toggle filesystem tree
vim.keymap.set("n", "<leader>o", "<CMD>Neotree source=document_symbols position=right toggle<CR>")   -- Toggle document symbols

-- Telescope
vim.keymap.set("n", "<leader>sf", "<CMD>Telescope find_files<CR>")   -- Search for files under cwd
vim.keymap.set("n", "<leader>su", "<CMD>Telescope live_grep<CR>")    -- Grep file contents under cwd
vim.keymap.set("n", "<leader>sg", "<CMD>call v:lua.G_telescope_live_grep_no_ut()<CR>")    -- Grep file contents under cwd, excluding 'unittest' directories
vim.keymap.set("n", "<leader>sb", "<CMD>Telescope buffers<CR>")      -- Search through buffers
vim.keymap.set("n", "<leader>sp", "<CMD>Telescope project<CR>")      -- Navigate saved projects
vim.keymap.set("n", "<leader>sn", "<CMD>Telescope notify<CR>")       -- Search through past notifications
vim.keymap.set("n", "<leader>sw", "<CMD>call v:lua.G_telescope_grep_string()<CR>")       -- Search the word under the cursor

-- Completion & LSP
vim.keymap.set("n", "<leader>dt", "<CMD>call v:lua.toggle_diagnostics()<CR>")   -- Toggle diagnostics
vim.keymap.set("n", "gD",         vim.lsp.buf.declaration)                      -- Go to function declaration
vim.keymap.set("n", "gd",         vim.lsp.buf.definition)                       -- Go to function definition
vim.keymap.set("n", "gl",         vim.diagnostic.open_float)                    -- Inspect diagnostic

-- Floating Terminal
vim.keymap.set("n", "<C-f>",      "<cmd>lua G_tmux_open()<cr>")  -- Toggle terminal
vim.keymap.set("i", "<C-f>",      "<cmd>lua G_tmux_open()<cr>")  -- Toggle terminal
vim.keymap.set("t", "<C-f>",      "<cmd>lua G_tmux_open()<cr>")  -- Toggle terminal
vim.keymap.set('t', '<C-j><C-k>', [[<C-\><C-n>]])  -- Switch to normal mode

-- Git Signs
vim.keymap.set("n", "<leader>gn", "<CMD>Gitsigns next_hunk<CR>")      -- Move to next hunk
vim.keymap.set("n", "<leader>gp", "<CMD>Gitsigns prev_hunk<CR>")      -- Move to previous hunk
vim.keymap.set("n", "<leader>gi", "<CMD>Gitsigns preview_hunk<CR>")   -- Diff the hunk
vim.keymap.set("n", "<leader>gr", "<CMD>Gitsigns reset_hunk<CR>")     -- Reset the hunk

-- Zen Modes
vim.keymap.set("n", "<leader>z", "<cmd>lua G_toggle_zen()<cr>")

-- Trouble
vim.keymap.set("n", "<leader>dp", "<CMD>TroubleToggle<CR>")   -- Toggle diagnostics panel

-- Debugging
vim.keymap.set("n", "<leader>db",  "<CMD>lua require('dap').toggle_breakpoint()<CR>")
vim.keymap.set("n", "<leader>dc",  "<CMD>lua require('dap').continue()<CR>")
vim.keymap.set("n", "<leader>dsn", "<CMD>lua require('dap').step_over()<CR>")
vim.keymap.set("n", "<leader>dsi", "<CMD>lua require('dap').step_into()<CR>")
vim.keymap.set("n", "<leader>dso", "<CMD>lua require('dap').step_out()<CR>")
vim.keymap.set("n", "<leader>dsi", "<CMD>lua require('dap').step_into()<CR>")

-- Fix terminal mode wierdness with shift key
vim.keymap.set("t", "<S-BS>", "<BS>")
vim.keymap.set("t", "<S-Space>", "<Space>")

-- Lazy Git
vim.api.nvim_set_keymap("n", "<leader>lg", "<cmd>lua G_lazygit_open()<CR>", { noremap = true, silent = true })

-- Font Resizing
vim.keymap.set("n", "<C-=>", "<cmd>lua G_change_font_size(1)<cr>")
vim.keymap.set("n", "<C-->", "<cmd>lua G_change_font_size(-1)<cr>")
