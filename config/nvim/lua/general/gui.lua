-- Function to retrieve console output
function execmd(cmd)
    local handle = assert(io.popen(cmd, 'r'))
    local output = assert(handle:read('*a'))
    handle.close()
    return output
end

-- Reduce default guifont size on 1920x1080 screens
if string.find(execmd("xdpyinfo | awk '/dimensions/{print $2}'"), "1920x1080") then
    vim.o.guifont = "JetBrainsMono Nerd Font:h7"
end

-- Neovide settings
-- resolve issue with CTRL-ALT key codes
vim.g.neovide_input_use_logo = true

-- set mode change cursor animation
vim.g.neovide_cursor_fix_mode = "sonicboom"

-- Set Terminal Colours
vim.g.terminal_color_0  = "#1C1F24"
vim.g.terminal_color_1  = "#FF6C6B"
vim.g.terminal_color_2  = "#98BE65"
vim.g.terminal_color_3  = "#ECBE7B"
vim.g.terminal_color_4  = "#51AFEF"
vim.g.terminal_color_5  = "#C678DD"
vim.g.terminal_color_6  = "#46D9FF"
vim.g.terminal_color_7  = "#B2B2B2"
vim.g.terminal_color_8  = "#5B6268"
vim.g.terminal_color_9  = "#FF9897"
vim.g.terminal_color_10 = "#B6D193"
vim.g.terminal_color_11 = "#F1D1A2"
vim.g.terminal_color_12 = "#85C7F3"
vim.g.terminal_color_13 = "#D7A0E7"
vim.g.terminal_color_14 = "#7DE4FF"
vim.g.terminal_color_15 = "#DFDFDF"
