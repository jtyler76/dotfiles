-- Indentation
vim.opt.smartindent = true   -- Indent based on context
vim.opt.tabstop     = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth  = 4
vim.opt.expandtab   = true

-- Backups
vim.opt.backup   = false   -- Don't create a backup file
vim.opt.swapfile = false   -- Don't create a swapfile

-- Colours
vim.opt.termguicolors = true   -- Allow for a larger range of colours

-- Searching
vim.opt.hlsearch   = false   -- Disable highlighting of search results
vim.opt.incsearch  = true    -- Enable highlighting of search results while typing
vim.opt.ignorecase = true    -- Ignore case when searching

-- Status Bars/Lines
vim.opt.showmode    = false   -- Don't show the current mode in the status line
vim.opt.ruler       = false   -- Prevent second status line from showing
vim.opt.laststatus  = 3       -- Disable per-window status lines
vim.opt.cmdheight   = 0       -- Hide command line until it's being used
vim.opt.showtabline = 1       -- Hide tab bar until you've got more than one tab

-- Window Titles
vim.opt.winbar = "%=%m %f"   -- Show window title at the top right of each window

-- Max column line
COLORCOLUMN = "81,101"
vim.opt.colorcolumn = COLORCOLUMN

-- Window Management
vim.opt.splitbelow  = true   -- Force vertical window splits to go below the current window
vim.opt.splitright  = true   -- Force horizontal window splits to go to the right of the current window
vim.opt.equalalways = true   -- Ensure windows are always created with equal size

-- Line Numbers
vim.opt.number         = true   -- Show line numbers
vim.opt.relativenumber = true   -- Show line numbers relative to your current position

-- Text Wrapping
vim.opt.wrap = false   -- Disable text wrapping by default

-- Scrolling
vim.opt.scrolloff = 5   -- Scroll when cursor reaches 5 lines from edge of screen

-- Sign Column
vim.opt.signcolumn = "yes"   -- Always show the sign column (for diagnostics and such)

-- Neovim Update Frequency
vim.opt.updatetime = 50   -- Neovim updates every 50ms

-- Command Reset Time
vim.opt.timeoutlen = 750   -- The amount of time Neovim will wait for you to complete a command

-- Show Whitespace
vim.opt.list = true                    -- Show whitespace characters
vim.opt.listchars = "tab:>-,space:·"   -- Show tabs with dashes, and spaces with dots

-- Wild Menu Autocompletion
vim.opt.wildmenu   = true
vim.opt.wildmode   = "list:full"
vim.opt.wildignore = "*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx"

-- Folding
vim.opt.foldenable = false   -- Disable folding by default

-- Neovide
vim.opt.guifont = "JetBrainsMono Nerd Font:h9"   -- The font and font size to use in Neovim GUIs
vim.g.neovide_padding_top    = 0
vim.g.neovide_padding_bottom = 0
vim.g.neovide_padding_right  = 0
vim.g.neovide_padding_left   = 0
vim.g.neovide_floating_blur_amount_x = 2.0
vim.g.neovide_floating_blur_amount_y = 2.0
vim.g.neovide_hide_mouse_when_typing = true
vim.g.neovide_confirm_quit = true
vim.g.neovide_cursor_vfx_mode = "ripple"
