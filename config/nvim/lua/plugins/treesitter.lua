return {
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        dependencies = {
            "nvim-treesitter/playground",
        },
        config = function()
            require("nvim-treesitter.configs").setup({
                playground = {
                    enable = true,
                    disable = {},
                    updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
                    persist_queries = false, -- Whether the query persists across vim sessions
                    keybindings = {
                        toggle_query_editor = 'o',
                        toggle_hl_groups = 'i',
                        toggle_injected_languages = 't',
                        toggle_anonymous_nodes = 'a',
                        toggle_language_display = 'I',
                        focus_language = 'f',
                        unfocus_language = 'F',
                        update = 'R',
                        goto_node = '<cr>',
                        show_help = '?',
                    },
                },
                ensure_installed = { "awk",
                                     "bash",
                                     "c",
                                     "cmake",
                                     "cpp",
                                     "css",
                                     "diff",
                                     "dockerfile",
                                     "fish",
                                     "git_config",
                                     "git_rebase",
                                     "gitattributes",
                                     "gitcommit",
                                     "gitignore",
                                     "go",
                                     "html",
                                     "ini",
                                     "java",
                                     "json",
                                     "llvm",
                                     "lua",
                                     "luadoc",
                                     "make",
                                     "markdown",
                                     "markdown_inline",
                                     "mermaid",
                                     "org",
                                     "python",
                                     "regex",
                                     "rust",
                                     "sql",
                                     "toml",
                                     "vim",
                                     "vimdoc",
                                     "yaml",
                                     "zig",
                },

                -- Install parsers synchronously (only applied to `ensure_installed`)
                sync_install = false,

                -- Automatically install missing parsers when entering buffer
                auto_install = true,

                autopairs = {
                    enable = true,
                },

                highlight = {
                    enable = true,
                    additional_vim_regex_highlighting = {"org"},
                },

                indent = {
                    enable = true,
                    disable = { "c", "cpp", "h", },  -- c-style comments broken
                },

                context_commentstring = {
                    enable = true,
                },
            })
        end,
    },

    {
        "nvim-treesitter/nvim-treesitter-context",
        config = function()
            require("treesitter-context").setup()

            vim.api.nvim_command("hi TreesitterContext cterm=italic gui=italic guibg=#29394F")
        end,
    },
}
