return {
    {
        "abecodes/tabout.nvim",
        event = "VeryLazy",
        config = function()
            require("tabout").setup({
                ignore_beginning = false,
            })
        end,
    },
}
