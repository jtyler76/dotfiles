return {
    {
        "max397574/better-escape.nvim",
        config = function()
            require("better_escape").setup({
                mappings = {
                    i = {
                        j = {
                            k = "<Esc>",
                        }
                    },
                    c = {
                        j = {
                            k = "<Esc>",
                        }
                    },
                    v = {
                        j = {
                            k = "<Esc>",
                        }
                    },
                    s = {
                        j = {
                            k = "<Esc>",
                        }
                    },
                    -- Disable for terminal mode
                    t = {
                        j = {
                            k = "jk",
                        }
                    }
                }
            })
        end,
    },
}
