return {
    {
        "folke/trouble.nvim",
        event = "VeryLazy",
        dependencies = {
            "nvim-tree/nvim-web-devicons",
        },
        config = function()
            require("trouble").setup({
                icons = false,   -- Seems to be some bug in the plugin...
                mode = "document_diagnostics",
            })
        end,
    },
}
