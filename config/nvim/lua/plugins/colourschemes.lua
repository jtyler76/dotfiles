return {
    {
        "EdenEast/nightfox.nvim",
        priority = 1000,
        lazy = false,
        config = function()
            -- Plugin Configuration
            require("nightfox").setup({
                options = {
                    terminal_colors = false,   --- use host terminal colours
                    styles = {
                        comments = "italic",
                        keywords = "bold",
                        types    = "bold",
                    },
                },
                specs = {
                    nightfox = {
                        syntax = {
                            builtin0 = "blue.bright",  -- ugly red by default
                        },
                    },
                },

                -- Manual overrides to fix the C colours
                groups = {
                    all = {
                        ["@function.macro.c"] = { fg = "palette.blue.bright" },
                        ["@keyword.return"] = { fg = "palette.magenta" },
                    },
                }
            })

            -- Set the colourscheme
            vim.cmd("colorscheme nightfox")

            -- If we're running in a terminal, set the background color to match neovim. This way
            -- the borders around neovim (which don't go to the edge of the terminal window) look
            -- nicer.
            if vim.env.TERM ~= nil then
                vim.fn.system(
                    string.format(
                        "kitty @ set-colors --all background=%s",
                        vim.fn.synIDattr(vim.fn.hlID("Normal"), "bg")
                    )
                )
            end

            -- Restore background colour when we close Neovim
            vim.cmd([[
                augroup kitty_background
                autocmd!
                autocmd VimLeave * lua vim.fn.system("kitty @ set-colors --all background=#282C34")
                augroup END
            ]])
        end,
    },
}

-- Notes on nightfox colorscheme modifications
--   Figuring out how to change colours was a bit tricky, so hopefully these notes will help speed
--   things up next time around.
--
--   Nightfox assigns colours based on various sources of information about a given token.
--       1) Neovim interpretation of what the token is
--       2) Treesitter interpretation of what the token is
--       3) LSP interpretation of what the token is
--
--   Although the Neovim docs say/imply that LSP SEMANTIC HIGHLIGHTS
--   (https://neovim.io/doc/user/lsp.html#lsp-semantic-highlight) won't override colours, they
--   definetly appear to do so (at least in this config).
--
--   As far as I can tell the colour choosing priority goes:
--       LSP -> Treesitter -> Builtin
--
--   So in order to map out how colours are being applied to a given token, we need to know how each
--   level assesses the token, and how Nightfox manages those interpretations.
--
--   King Folke was nice enough to add the ':Inspect' command to Neovim, which can be run to give
--   all of this information for the token your cursor is sitting on.
--      Treesitter -> Treesitter
--      Semantic Tokens -> LSP
--
--   This feature also shows the highlights applied for each categorization and the associated
--   priorities so you can map out where your troublesome colour might be coming from.
--
--   It also shows the mapping from the LSP label to the Treesitter label.
--
--   The following file shows all of the Nightfox mappings of Treesitter labels to palette entries:
--       https://github.com/EdenEast/nightfox.nvim/blob/main/lua/nightfox/group/modules/treesitter.lua
--
--   The following file shows all of the palette entries:
--       https://github.com/EdenEast/nightfox.nvim/blob/main/lua/nightfox/palette/nightfox.lua
--
--   And finally the Nightfox docs state how to mess with all of these things:
--       https://github.com/EdenEast/nightfox.nvim/blob/main/usage.md#configuration
