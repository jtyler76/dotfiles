return {
    {
        "nvim-neo-tree/neo-tree.nvim",
        event = "VeryLazy",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-web-devicons",
            "MunifTanjim/nui.nvim",
            {
                's1n7ax/nvim-window-picker',
                name = 'window-picker',
                event = 'VeryLazy',
                version = '2.*',
                config = function()
                    require 'window-picker'.setup({
                        hint = 'statusline-winbar',
                        picker_config = {
                            statusline_winbar_picker = {
                                use_winbar = 'smart',
                            },
                        },
                        filter_rules = {
                            include_current_win = false,
                            autoselect_one = true,
                            -- filter using buffer options
                            bo = {
                                -- if the file type is one of following, the window will be ignored
                                filetype = { 'neo-tree', "neo-tree-popup", "notify" },
                                -- if the buffer type is one of following, the window will be ignored
                                buftype = { 'terminal', "quickfix" },
                            },
                        },
                    })
                end,
            },
        },
        config = function()
            -- Remove deprecated commands
            vim.cmd([[ let g:neo_tree_remove_legacy_commands = 1 ]])

            require("neo-tree").setup({
                close_if_last_window = true,
                enable_git_status = true,
                enable_diagnostics = false,
                open_files_do_not_replace_types = { "terminal", "trouble", "qf" },
                use_libuv_file_watcher = true,
                filesystem = {
                    filtered_items = {
                        hide_dotfiles = false,
                        hide_gitignored = false,
                    },
                    follow_current_file = {
                        enabled = true,
                    },
                    window = {
                        mappings = {
                            -- disable fuzzy finder
                            ["/"] = "noop"
                        }
                    }
                },
                default_component_configs = {
                    git_status = {
                        symbols = {
                            -- Change type
                            added     = "✚",
                            modified  = "",
                            deleted   = "✖",
                            renamed   = "󰜥",
                            -- Status Types
                            untracked = "◌",
                            ignored   = "",
                            unstaged  = "",
                            staged    = "",
                            conflict  = "",
                        },
                    },
                },
                window = {
                    mappings = {
                        ["<cr>"] = "open_with_window_picker",
                    },
                },
                sources = { "document_symbols", "filesystem" },
            })

            vim.api.nvim_command("hi NeoTreeGitUntracked guifg=#63CDCF")

            function G_neotree_refresh()
                require("neo-tree.sources.manager").refresh("filesystem")
            end

            vim.cmd([[autocmd! BufWritePost * lua G_neotree_refresh()]])
        end,
    }
}
