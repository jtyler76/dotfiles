return {
    {
        "folke/zen-mode.nvim",
        event = "VeryLazy",
        config = function()
            require("zen-mode").setup({
                window = {
                    backdrop = 0.98,
                    width = .60,
                    height = 1,

                    options = {
                        signcolumn = "yes",
                        number = true,
                        relativenumber = true,
                    },
                },

                plugins = {
                    gitsigns = { enabled = true, },
                    kitty = {
                        enabled = false,
                        font = "+4",
                    },
                    options = {
                        enabled = true,
                        ruler = false,
                        showcmd = false,
                    },
                    twilight = { enabled = false, },
                },

                -- callback where you can add custom code when the Zen window opens
                on_open = function(win)
                    G_change_font_size(3)
                    vim.opt.colorcolumn = ""
                    require('lualine').hide()
                end,

                -- callback where you can add custom code when the Zen window closes
                on_close = function()
                    G_change_font_size(-3)
                    vim.opt.colorcolumn = COLORCOLUMN
                    require('lualine').hide({ unhide = true })
                    require('gitsigns').toggle_current_line_blame()
                end,
            })

            -- The gitsigns line blame needs to be disabled before the zen mode
            -- toggle, presumably due to some internal race condition. If the
            -- gitsigns toggle is in the on_open() handler as opposed to here,
            -- the virtual text gets frozen in place and doesn't go away or
            -- follow the cursor.
            function G_toggle_zen()
                require('gitsigns').toggle_current_line_blame()
                require("zen-mode").toggle()
            end
        end,
    },
}
