return {
    {
        "akinsho/toggleterm.nvim",
        version = "*",
        config = function()
            require("toggleterm").setup({
                direction = "float",
                float_opts = {
                    border = "curved",
                    height = function(term)
                        return math.floor(vim.o.lines * 0.92)
                    end,
                    width = function(term)
                        return math.floor(vim.o.columns * 0.95)
                    end,
                },
                winbar = {
                    enabled = true,
                    name_formatter = function(term) --  term: Terminal
                        return term.name
                    end
                },
                on_close = function()
                    require("neo-tree.sources.manager").refresh("filesystem")
                end,
            })

            function G_lazygit_open()
                local Terminal = require("toggleterm.terminal").Terminal
                local lazygit = Terminal:new({ cmd = "lazygit", hidden = false, count = 2 })
                lazygit:toggle()
            end

            local Terminal = require("toggleterm.terminal").Terminal
            local tmux = Terminal:new({ cmd = "tmux", hidden = false, count = 1 })

            function G_tmux_open()
                tmux:toggle()
            end
        end,
    },
}
