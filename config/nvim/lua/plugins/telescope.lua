return {
    {
        "nvim-telescope/telescope.nvim",
        event = "VeryLazy",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        config = function()
            local telescope = require("telescope")

            -- Setup Telescope
            telescope.setup({
                extensions = {
                    project = {
                        on_project_selected = function(prompt_bufnr)
                            local project_actions = require("telescope._extensions.project.actions")
                            project_actions.change_working_directory(prompt_bufnr, false)
                            vim.api.nvim_command("Neotree source=filesystem position=left toggle")
                        end
                    }
                }
            })

            -- Load Extensions
            telescope.load_extension("media_files")
            telescope.load_extension("project")
            telescope.load_extension("ui-select")
            telescope.load_extension("noice")
            telescope.load_extension("notify")

            function G_telescope_live_grep_no_ut()
                require("telescope.builtin").live_grep({
                    prompt_title = "Live Grep (!unittests)",
                    glob_pattern = "!unittests",
                })
            end

            function G_telescope_grep_string()
                require("telescope.builtin").grep_string()
            end
        end,
    },
    { "nvim-telescope/telescope-media-files.nvim" },
    { "nvim-telescope/telescope-project.nvim" },
    { "nvim-telescope/telescope-ui-select.nvim" },
}
