return {
    {
        "windwp/nvim-autopairs",
        event = "VeryLazy",
        config = function()
            local npairs = require("nvim-autopairs")
            local rule = require("nvim-autopairs.rule")
            local cond = require("nvim-autopairs.conds")

            npairs.setup()

            -- If you want insert `(` after select function or method item
            local cmp_autopairs = require('nvim-autopairs.completion.cmp')
            local cmp = require('cmp')
            cmp.event:on(
                'confirm_done',
                cmp_autopairs.on_confirm_done()
            )

            -- C comments autocompletion rules
            npairs.add_rule(rule("/* "," */", {"c", "cpp", "h", "rust"}))   -- Auto-complete single line c-style comments

            -- npairs.add_rule(
            --     rule("/*", "*/", {"c", "cpp", "h", "rust"})
            --         -- :use_key("<Space>")
            --         -- :with_move(cond.none())
            -- )
        end,
    },
}
