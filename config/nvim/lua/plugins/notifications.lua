return {
    {
        "rcarriga/nvim-notify",
        event = "VeryLazy",
        config = function()
            local notify = require("notify")
            vim.notify = require("notify")

            notify.setup({
                render = "compact",
                stages = "fade_in_slide_out",
                timeout = 0.25,
            })
        end,
    },
}
