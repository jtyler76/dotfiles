return {
    {
        'glepnir/dashboard-nvim',
        event = 'VimEnter',
        config = function()
            require('dashboard').setup({
                theme = "hyper",
                config = {
                    header = {
                        "                                                     ",
                        "  ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗ ",
                        "  ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║ ",
                        "  ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║ ",
                        "  ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║ ",
                        "  ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║ ",
                        "  ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝ ",
                        "                                                     ",
                    },
                    shortcut = {
                        {
                            desc = 'New Buffer ',
                            group = '@property',
                            action = 'enew',
                            key = 'n'
                        },
                        {
                            desc = 'Update Plugins ',
                            group = '@property',
                            action = 'Lazy update',
                            key = 'u'
                        },
                    },
                    project = {
                        enable = true,
                        limit = 10,
                        action = function(path)
                            vim.api.nvim_command("cd " .. path)
                            vim.api.nvim_command("Neotree source=filesystem position=left toggle")
                        end,
                    },
                    footer = {}
                },
            })
        end,
        dependencies = { {'nvim-tree/nvim-web-devicons'}}
    },
}
