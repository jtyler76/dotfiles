return {
    {
        "neovim/nvim-lspconfig",  -- Better LSP Configuration
        opts = {
            setup = {
                -- Rustaceanvim requires that this be disabled
                rust_analyzer = function()
                    return true
                end,
            }
        },
        dependencies = {
            "hrsh7th/nvim-cmp",      -- Neovim Auto-Completion Plugin
            "hrsh7th/cmp-nvim-lsp",  -- (Required)

            {
                "williamboman/mason.nvim", -- Tool for installing LSP/DAP/Lint/Formatters
                build = function()
                    pcall(vim.cmd, "MasonUpdate")
                end,
            },
            "williamboman/mason-lspconfig.nvim",
            {
                "mrcjkb/rustaceanvim",  -- Rust Tooling
                version = "^5",         -- Recommended
                lazy = false,           -- This plugin is already lazy
            },
            {
                "felpafel/inlay-hint.nvim",
                event = "LspAttach",
                config = true,
            },
            "L3MON4D3/LuaSnip",          -- Required
            "saadparwaiz1/cmp_luasnip",  -- Optional
        },
        config = function()
            local luasnip = require("luasnip")

            require("mason").setup()
            require("mason-lspconfig").setup()

            -- Mason configurations needs to come before lsp-zero
            require('mason.settings').set({
                ui = {
                    keymaps = {
                        apply_language_filter = "<leader>mf",
                    },
                },
            })

            local cmp_nvim_lsp = require("cmp_nvim_lsp")
            local lspconfig = require("lspconfig")
            local lspconfig_defaults = lspconfig.util.default_config
            lspconfig_defaults.capabilities = vim.tbl_deep_extend(
                "force",
                lspconfig_defaults.capabilities,
                cmp_nvim_lsp.default_capabilities()
            )

            -- Enable features only when a language server is active
            vim.api.nvim_create_autocmd('LspAttach', {
                desc = 'LSP actions',
                callback = function(event)
                    local opts = {buffer = event.buf}

                    vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
                    vim.keymap.set('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', opts)
                    vim.keymap.set('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', opts)
                    vim.keymap.set('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>', opts)
                    vim.keymap.set('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>', opts)
                    vim.keymap.set('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>', opts)
                    vim.keymap.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)
                    vim.keymap.set('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
                    vim.keymap.set({'n', 'x'}, '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
                    vim.keymap.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)

                    -- NOTE: from previous config
                    -- vim.keymap.set("n", "ga", "<cmd>lua vim.lsp.buf.code_action()<cr>", {buffer = true})
                    -- vim.keymap.set("n", "gr", "<cmd>Telescope lsp_references<cr>", {buffer = true})
                    -- vim.keymap.set("n", "gR", "<cmd>lua vim.lsp.buf.rename()<cr>", {buffer = true})

                    -- Enable inlay hints
                    local client = vim.lsp.get_client_by_id(event.data.client_id)
                    if client.server_capabilities.inlayHintProvider then
                        vim.lsp.inlay_hint.enable(true)
                    end
                end,
            })

            -- Setup Language Servers
            --   See list of language servers here:
            --     https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md

            -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md#ccls
            lspconfig.ccls.setup({
                on_attach = function() end,
                capabilities = cmp_nvim_lsp.default_capabilities(),
                root_dir = function(fname)
                    local util = require("lspconfig.util")
                    return util.root_pattern("compile_commands.json",
                                             ".ccls",
                                             ".ccls-cache",
                                             ".git",
                                             "common.mk",
                                             "Makefile")(fname)
                        or vim.fn.expand("%:p:h") -- returns parent directory of current file
                end,
                flags     = {},
            })

            -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md#lua_ls
            lspconfig.lua_ls.setup({
                settings = {
                    Lua = { diagnostics = { globals = { "vim" } } }
                }
            })

            local cmp = require("cmp")
            cmp.setup({
                sources = {
                    {name = 'nvim_lsp'},
                },
                snippet = {
                    expand = function(args)
                        -- You need Neovim v0.10 to use vim.snippet
                        vim.snippet.expand(args.body)
                    end,
                },
                completion = {
                    autocomplete = false,  -- Don't show autocompletion automatically
                },

                mapping = {
                    ["<C-Space>"] = cmp.mapping.complete(),                  -- Open autocompletion
                    ["<C-k>"]     = cmp.mapping.select_prev_item(),          -- Move to previous item
                    ["<C-j>"]     = cmp.mapping.select_next_item(),          -- Move to next item
                    ["<C-h>"]     = cmp.mapping.scroll_docs(-1),             -- Scroll up documentation
                    ["<C-l>"]     = cmp.mapping.scroll_docs(1),              -- Scroll down documentation
                    ["<CR>"]      = cmp.mapping.confirm { select = true },   -- Accept currently selected item
                    ["<Tab>"]     = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_next_item()
                        elseif luasnip.expand_or_jumpable() then
                            luasnip.expand_or_jump()
                        else
                            fallback()
                        end
                    end, { "i", "s", }),
                    ["<S-Tab>"]   = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_prev_item()
                        elseif luasnip.jumpable(-1) then
                            luasnip.jump(-1)
                        else
                            fallback()
                        end
                    end, { "i", "s", }),
                },

                -- Inlay Settings
                require("inlay-hint").setup({
                    virt_text_pos = "eol",
                })
            })
        end,
    },
}

-- NOTE: Old config below

-- return {
--     {
--         "VonHeikemen/lsp-zero.nvim",
--         branch = "v4.x",
--         dependencies = {
--             -- LSP Support
--             "neovim/nvim-lspconfig",  -- Better LSP configuration
--             {
--                 "williamboman/mason.nvim",  -- Tool for installing LSP/DAP/Lint/Formatters
--                 build = function()
--                     pcall(vim.cmd, "MasonUpdate")
--                 end,
--             },
--             "williamboman/mason-lspconfig.nvim",  -- Extension to bridge lspconfig and mason
--
--             -- Autocompletion
--             "hrsh7th/nvim-cmp",           -- Neovim autocompletion plugin
--             "hrsh7th/cmp-nvim-lsp",       -- Required
--             "hrsh7th/cmp-buffer",         -- Optional
--             "hrsh7th/cmp-path",           -- Optional
--             "saadparwaiz1/cmp_luasnip",   -- Optional
--             "hrsh7th/cmp-nvim-lua",       -- Optional
--
--             -- Snippets
--             "L3MON4D3/LuaSnip",             -- Required
--             "rafamadriz/friendly-snippets", -- Optional
--
--             -- Debugging
--             "mfussenegger/nvim-dap",         -- Debugger adaptor protocol
--             "jay-babu/mason-nvim-dap.nvim",  -- Bridge between DAP and Mason
--     	    "nvim-neotest/nvim-nio",         -- dependency of nvim-dap-ui
--             "rcarriga/nvim-dap-ui",          -- Nice debugging UI
--
--             -- Rust
--             -- "simrat39/rust-tools.nvim"    -- Rust setup & improvements
--             {
--                 "mrcjkb/rustaceanvim",
--                 version = "^5", -- Recommended
--                 lazy = false,  -- This plugin is already lazy
--             },
--         },
--         config = function()
--             -- Mason configurations needs to come before lsp-zero
--             require('mason.settings').set({
--                 ui = {
--                     keymaps = {
--                         apply_language_filter = "<leader>mf",
--                     },
--                 },
--             })
--
--             local lsp = require('lsp-zero').preset({
--                 name = 'minimal',
--                 set_lsp_keymaps = false,       -- Manually set keymaps in keymaps.lua
--                 manage_nvim_cmp = true,        -- Manage the nvim_cmp plugin for us (config below)
--                 suggest_lsp_servers = false,   -- Don't suggest servers when opening files
--             })
--
--             -- Override keymaps
--             lsp.on_attach(function(client, bufnr)
--                 lsp.default_keymaps({buffer = bufnr})
--
--                 vim.keymap.set("n", "ga", "<cmd>lua vim.lsp.buf.code_action()<cr>", {buffer = true})
--                 vim.keymap.set("n", "gr", "<cmd>Telescope lsp_references<cr>", {buffer = true})
--                 vim.keymap.set("n", "gR", "<cmd>lua vim.lsp.buf.rename()<cr>", {buffer = true})
--             end)
--
--             local cmp = require("cmp")
--             local luasnip = require("luasnip")
--             local lspconfig = require("lspconfig")
--             local cmp_nvim_lsp = require("cmp_nvim_lsp")
--
--             -- nvim_cmp is managed through lsp-zero, but we can override some configurations here
--             lsp.setup_nvim_cmp({
--                 completion = {
--                     autocomplete = false,   -- Don't show autocompletion automatically
--                 },
--
--                 mapping = {
--                     ["<C-Space>"] = cmp.mapping.complete(),                  -- Open autocompletion
--                     ["<C-k>"]     = cmp.mapping.select_prev_item(),          -- Move to previous item
--                     ["<C-j>"]     = cmp.mapping.select_next_item(),          -- Move to next item
--                     ["<C-h>"]     = cmp.mapping.scroll_docs(-1),             -- Scroll up documentation
--                     ["<C-l>"]     = cmp.mapping.scroll_docs(1),              -- Scroll down documentation
--                     ["<CR>"]      = cmp.mapping.confirm { select = true },   -- Accept currently selected item
--                     ["<Tab>"]     = cmp.mapping(function(fallback)
--                         if cmp.visible() then
--                             cmp.select_next_item()
--                         elseif luasnip.expand_or_jumpable() then
--                             luasnip.expand_or_jump()
--                         else
--                             fallback()
--                         end
--                     end, { "i", "s", }),
--                     ["<S-Tab>"]   = cmp.mapping(function(fallback)
--                         if cmp.visible() then
--                             cmp.select_prev_item()
--                         elseif luasnip.jumpable(-1) then
--                             luasnip.jump(-1)
--                         else
--                             fallback()
--                         end
--                     end, { "i", "s", }),
--                 },
--             })
--
--             lsp.ensure_installed({
--                 -- "clangd",          -- C/C++ LSP
--                 -- "bashls",          -- Bash LSP
--                 -- "pyright",         -- Python
--                 "lua_ls",          -- Lua
--                 -- "asm_lsp",         -- Assembly
--                 -- "jsonls",          -- JSON
--                 -- "ltex",            -- Markdown/Latex
--                 "rust_analyzer",   -- Rust
--             })
--
--             -- C CCLS Config. Mason refuses to configure CCLS automatically, so we have to do it
--             -- manually.
--             lspconfig['ccls'].setup {
--                 on_attach = function() end,
--                 capabilities = cmp_nvim_lsp.default_capabilities(),
--                 root_dir = function(fname)
--                     local util = require("lspconfig.util")
--                     return util.root_pattern("compile_commands.json",
--                                              ".ccls",
--                                              ".ccls-cache",
--                                              ".git",
--                                              "common.mk",
--                                              "Makefile")(fname)
--                         or vim.fn.expand("%:p:h") -- returns parent directory of current file
--                 end,
--                 flags     = {},
--             }
--
--             -- RUST
--             --   rust-analyzer.completion.fullFunctionSignatures.enable (default: false)
--             --       Whether to show full function/method signatures in completion docs.
--             --   rust-analyzer.hover.actions.enable (default: true)
--             --       Whether to show HoverActions in Rust files.
--             --   rust-analyzer.hover.actions.references.enable (default: false)
--             --       Whether to show References action. Only applies when rust-analyzer.hover.actions.enable is set.
--             --   rust-analyzer.hover.documentation.enable (default: true)
--             --       Whether to show documentation on hover.
--
--             -- Fix the 'vim not defined' issue in the LSP
--             lsp.configure("lua_ls", {
--                 settings = {
--                     Lua = { diagnostics = { globals = { "vim" } } }
--                 }
--             })
--
--             lsp.setup()
--
--             -- -- Rust & Rust-Tools configuration
--             -- require('mason-lspconfig').setup({
--             --     handlers = {
--             --         lsp.default_setup,
--             --         rust_analyzer = function()
--             --             local rt = require("rust-tools")
--             --             rt.setup({
--             --                 server = {
--             --                     on_attach = function(_, bufnr)
--             --                         -- Hover actions
--             --                         vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
--             --                         -- Code action groups
--             --                         vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
--             --                     end,
--             --                 },
--             --             })
--             --         end,
--             --     }
--             -- })
--             require("mason-lspconfig").setup_handlers {
--                 ["rust_analyzer"] = function() end,
--             }
--
--             -- This need to be included after LSP
--             local dap = require("dap")
--             local dapui = require("dapui")
--
--             -- Configure DAP
--             require("mason-nvim-dap").setup({
--                 ensure_installed = {
--                     "cppdbg",   -- C/C++/Rust
--                     "bash",     -- Bash
--                     -- "python",   -- Python
--                 },
--                 automatic_installation = true,
--                 automatic_setup = true,
--                 handlers = {},
--             })
--
--             -- Setup DAP UI
--             dapui.setup()
--
--             dap.listeners.after.event_initialized["dapui_config"] = function()
--                 dapui.open()
--             end
--             dap.listeners.before.event_terminated["dapui_config"] = function()
--                 dapui.close()
--             end
--             dap.listeners.before.event_exited["dapui_config"] = function()
--                 dapui.close()
--             end
--
--             -- Breakpoint symbols
--             vim.fn.sign_define('DapBreakpoint', {text='', texthl='Error',      linehl='', numhl=''})
--             vim.fn.sign_define('DapStopped',    {text='󱅥', texthl='Identifier', linehl='', numhl=''})
--         end,
--     },
-- }
