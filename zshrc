# Path to your oh-my-zsh installation.
export ZSH="/home/jtyler/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# ZSH_THEME="jetyler"

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
# plugins=(git git-prompt)

source $ZSH/oh-my-zsh.sh

## Disable shared history
unsetopt share_history
PROMPT_COMMAND="hash -r"

## Disable hash table search
##   This should make is so that zsh doesn't do any hashing at all, and always looks for each
##   executable through the PATH each time. However, for some reason this doesn't resolve the
##   issue. Keeping this here anyways.
setopt nohashexecutablesonly

## Add a pre-exec hook to run 'rehash' before every command as a means of manually blowing out
## the cache. This solves the caching issue, but may have very slight performance consequences.
function preexec_hook() {
    rehash
}
preexec_functions=(preexec_hook)

# General Aliases
alias tmp="cd `mktemp -d`"
alias lg="lazygit"

# Clipboard Alias
if [[ "${XDG_SESSION_TYPE}" == "x11" ]]; then
    alias clip="xclip -r -sel clip"
elif [[ "${XDG_SESSION_TYPE}" == "wayland" ]]; then
    alias clip="wl-copy"
fi


# Editor Aliases
alias e="emacs -nw"
alias v="nvim"

# Git Aliases
alias git='git --no-pager'
alias gremote='git rev-parse --abbrev-ref --symbolic-full-name @{u}'
alias glog='git --no-pager log --oneline $(gremote)~1..'
alias glogg='git --no-pager log $(gremote)~1..'
alias mdiff='git difftool --dir-diff -t meld'
alias glines='git --no-pager diff --stat $(gremote)..HEAD'

## GDB
alias gdb='gdb --quiet'

## 'icat' alias specifically for the 'kitty' terminal
if [[ "${TERM}" == "xterm-kitty" ]]; then
    alias icat='kitty +kitten icat'
fi

## disable python virtual environment prompt prefix
export VIRTUAL_ENV_DISABLE_PROMPT=1

## add the 'doom' binary to PATH
export PATH=${HOME}/.emacs.d/bin:${PATH}
export PATH=${HOME}/.local/bin:${PATH}

## Include local configuration
if [ -f ${HOME}/.zshrc_local ]; then
    source ${HOME}/.zshrc_local
fi

## Run Neofetch when the shell opens
if [ -n "$TMUX" ]; then
    clear
fi
neofetch --disable gpu

eval "$(starship init zsh)"
