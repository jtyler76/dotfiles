-- :help options

local options = {
    autoindent     = true,               -- auto indent
    smarttab       = true,               -- insert tabstop number of space when the tab key is pressed
    backup         = false,              -- don't create a backup file
    termguicolors  = true,               -- set term gui colors
    fileencoding   = "utf-8",            -- use UTF-8 encoding
    hlsearch       = true,               -- highlight all matches on previous search pattern
    incsearch      = true,               -- highlight as you search
    ignorecase     = true,               -- ignore case in search patterns
    pumheight      = 10,                 -- pop up menu height
    showmode       = false,              -- disable 'INSERT' etc.
    showtabline    = 2,                  -- always show tabs
    smartcase      = true,               -- uppercase letter in search only match to uppercase
    showmatch      = true,               -- match words as you search
    smartindent    = true,               -- improve indentation
    splitbelow     = true,               -- force horizontal splits to go below current window
    splitright     = true,               -- force vertical splits to go to the right
    swapfile       = false,              -- don't create a swapfile
    timeoutlen     = 750,                -- time to wait for keybindings in ms
    undofile       = true,               -- enable persistent undo
    updatetime     = 300,                -- faster completion
    expandtab      = true,               -- converts tabs to spaces
    shiftwidth     = 4,                  -- number of spaces for indentation
    tabstop        = 4,                  -- set tabs to 4 spaces
    cursorline     = true,               -- highlight the current line
    number         = true,               -- set numbered lines
    relativenumber = true,               -- set relative numbers
    wrap           = false,              -- don't wrap lines
    scrolloff      = 5,                  -- scroll as you approach 5 lines from end of page
    guifont        = "Fira Code:h8",     -- set the font
    signcolumn     = "yes",              -- always show sign column
    syntax         = "off",              -- code comments same font as code
    filetype       = "on",               -- enable file type detection
    compatible     = false,              -- disable vi compatibility
    ruler          = false,              -- prevent second status bar from showing

-- show whitespace characters
    list           = true,
    listchars      = "tab:>-,space:·",

-- wild menu, autocompletion similar to bash completion when executing commands
    wildmenu       = true,
    wildmode       = "list:longest",
    wildignore     = "*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx"
}

for k,v in pairs(options) do
    vim.opt[k] = v
end

vim.cmd [[set iskeyword+=-]]

-- enable filetype specific plugins
vim.cmd [[filetype plugin on]]

-- indentation
vim.cmd [[filetype indent on]]

-- remove trailing whitespace on save
vim.cmd [[autocmd BufWritePre * :%s/\s\+$//e]]

-- code folding
vim.cmd [[
"" ensure vim file type is detected
augroup filetype
    au! BufRead,BufNewFile *.vim set filetype=vim
augroup END

"" VIM folding method
autocmd FileType vim set foldmethod=marker

"" C folding method
autocmd FileType c set foldmethod=syntax
]]
