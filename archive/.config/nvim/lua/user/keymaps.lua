local opts      = { noremap = true, silent = true }
local term_opts = { silent = true }

local keymap = vim.api.nvim_set_keymap

-- Set leader key
keymap('', '<Space>', '<Nop>', opts)
vim.g.mapleader      = ' '
vim.g.maplocalleader = ' '

-- Modes
--     normal mode       = 'n'
--     insert mode       = 'i'
--     visual mode       = 'v'
--     visual block mode = 'x'
--     term mode         = 't'
--     command mode      = 'c'

--- NORMAL MODE MAPPINGS ---
-- Window Management - Creation
keymap('n', '<leader>wv', ':vsplit<CR>', opts)
keymap('n', '<leader>ws', ':split<CR>',  opts)

-- Window Management - Deletion
keymap('n', '<leader>wq', '<C-w>q', opts)

-- Window Management - Navigation
keymap('n', '<leader>wh', '<C-w>h', opts)
keymap('n', '<leader>wj', '<C-w>j', opts)
keymap('n', '<leader>wk', '<C-w>k', opts)
keymap('n', '<leader>wl', '<C-w>l', opts)

-- Window Management - Resizing
keymap('n', '<C-Up>',    ':resize +2<CR>',          opts)
keymap('n', '<C-Down>',  ':resize -2<CR>',          opts)
keymap('n', '<C-Left>',  ':vertical resize -2<CR>', opts)
keymap('n', '<C-Right>', ':vertical resize +2<CR>', opts)

-- Buffer Management - Switching
keymap('n', '<S-l>', ':bnext<CR>',     opts)
keymap('n', '<S-h>', ':bprevious<CR>', opts)

-- File Management - Tree
keymap('n', '<leader>e', ':NvimTreeToggle<CR>', opts)

-- Text Movement - Move line up or down
keymap('n', '<A-j>', ':m .+1<CR>==', opts)
keymap('n', '<A-k>', ':m .-2<CR>==', opts)

-- Fuzzy Finder
keymap('n', '<leader>sf', ':Telescope find_files<CR>', opts)
keymap('n', '<leader>sg', ':Telescope live_grep<CR>',  opts)

-- Floating Terminal
keymap('n', '<leader>ft', ':FloatermToggle<CR>', opts)
keymap('n', '<leader>fn', ':FloatermNew<CR>',    opts)
keymap('n', '<leader>fk', ':FloatermKill<CR>',   opts)
keymap('n', '<leader>fh', ':FloatermPrev<CR>',   opts)
keymap('n', '<leader>fl', ':FloatermNext<CR>',   opts)


--- INSERT MODE MAPPINGS ---
-- Enter NORMAL mode with jk
keymap('i', 'jk', '<ESC>', opts)


--- VISUAL MODE MAPPINGS ---
-- Persistent Indenting
keymap('v', '<', '<gv', opts)
keymap('v', '>', '>gv', opts)

-- Moving text up and down
keymap("x", "<A-k>", ':m-2<CR>gv=gv',   opts)
keymap('x', '<A-j>', ':m\'>+<CR>gv=gv', opts)

-- Persistent paste (don't replace buffer contents on paste)
keymap('v', 'p', '"_dP', opts)


--- TEMINAL MODE MAPPINGS ---
-- Get to NORMAL mode
keymap('t', '<ESC>', '<C-\\><C-n>', term_opts)
keymap('t', 'jk',    '<C-\\><C-n>', term_opts)

-- Floating Terminal
keymap('t', '<C-h>', '<C-\\><C-n>:FloatermPrev<CR>', term_opts)
keymap('t', '<C-l>', '<C-\\><C-n>:FloatermNext<CR>', term_opts)

