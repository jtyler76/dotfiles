""" This config uses Vim-Plug for plugin management, and thus it must be
""" installed in order to use the config. See their github page for
""" installation instructions: https://github.com/junegunn/vim-plug

" PLUGINS {{{

"" Include plugins.
call plug#begin()
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'baskerville/bubblegum'
Plug 'sainnhe/sonokai'
Plug 'romgrk/doom-one.vim'
Plug 'voldikss/vim-floaterm'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'sakhnik/nvim-gdb', { 'do': ':!./install.sh' }
call plug#end()

"" Enable list of buffers at the top of the window.
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'default'

"" Set the airline theme.
let g:airline_theme='bubblegum'

"" Floaterm Settings
let g:floaterm_width = 0.95
let g:floaterm_height = 0.95
let g:floaterm_title = '($1/$2)'

"" Setup colorizer.
set termguicolors
lua require'colorizer'.setup()

" }}}

" GENERAL {{{

"" Alternate escape key
inoremap jk <ESC>

"" Disable vi compatibility since it can cause some issues.
set nocompatible

"" Enable file type detection.
filetype on

"" Enable plugins and load plugins for the detected file type.
filetype plugin on

"" Turn on syntax highlighting.
syntax on

"" Don't save backup files.
set nobackup

"" Do not let cursor scroll too high or low on the page.
set scrolloff=5

"" Prevent lines from wrapping.
set nowrap

"" Show the mode you are in on the last line.
set showmode

"" Enable unicode characters.
set encoding=utf-8

"" Set color scheme.
set background=dark
"colorscheme bubblegum-256-dark
colorscheme sonokai

"" Turn on lazy redraw for better performance.
set lazyredraw

"" prevent second status bar from showing
set noshowmode
set noruler

"" Remove trailing whitespace on save.
autocmd BufWritePre * :%s/\s\+$//e

"" Show whitespace characters
set list
set listchars=tab:>-,space:·

" }}}

" LINE NUMBERS {{{

"" Display numbers in the left margin.
set number

"" Show relative numbers in the margin.
set relativenumber

" }}}

" INDENTATION {{{

"" Load an indent file for the detected file type.
filetype indent on

"" Turn on auto indent.
set autoindent

"" Set shift width to 4 spaces.
set shiftwidth=4

"" Set tab width to 4 spaces.
set tabstop=4

"" Use spaces instead of tabs.
set expandtab

"" Insert tabstop number of space when the tab key is pressed.
set smarttab

"" }}}

" SEARCHING  {{{

"" Use highlighting when doing a search.
set hlsearch

"" Highlight characters as you search with /.
set incsearch

"" Lowercase letters in your search text will match to capital letters in the
"" document.
set ignorecase

"" Uppercase letters in your search only match to upper case letters in the
"" document.
set smartcase

"" Show matching words during your search.
set showmatch

"  }}}

" CODE FOLDING  {{{

"" Make sure that *.vim files get detected as the vim filetype.
augroup filetype
    au! BufRead,BufNewFile *.vim    set filetype=vim
augroup END

"" Set the folding method for vim files to marker.
autocmd FileType vim set foldmethod=marker

"" Set the folding method for C files to syntax.
autocmd FileType c set foldmethod=syntax

"  }}}

" EXECUTING COMMANDS {{{

"" Set the number of commands to save in history, beyond default of 20.
set history=1000

"" Show partial command in bottom line as you type.
set showcmd

"" Enable wildmenu, which enables auto completion similar to bash completion
"" when executing a command.
set wildmenu
set wildmode=list:longest
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

" }}}

" KEY BINDINGS {{{

"" Set the leader key.
let mapleader = " "

"" Shorten the time window for typing out key binding characters. It defaults
"" to 1000ms.
set timeoutlen=500

"" Floating terminal keybindings.
map <leader>ff :FloatermNew<CR>
map <leader>ft :FloatermToggle<CR>
map <leader>fn :FloatermNext<CR>
map <leader>fp :FloatermPrev<CR>
map <leader>fk :FloatermKill<CR>

"" Mapping Terminal Mode ESC Key to Enter Normal Mode
tnoremap <Esc> <C-\><C-n>

" }}}

" NEOVIDE TERMINAL SETTINGS {{{

"" This setting resolves an issue with CTRL-ALT key codes.
let g:neovide_input_use_logo=v:true

"" Set Neovide ESC/Insert Cursor Animation
let g:neovide_cursor_vfx_mode = "sonicboom"

"" Normal Colors
let g:terminal_color_0  = '#1C1F24'  | " black
let g:terminal_color_1  = '#FF6C6B'  | " red
let g:terminal_color_2  = '#98BE65'  | " green
let g:terminal_color_3  = '#DA8548'  | " yellow
let g:terminal_color_4  = '#51AFEF'  | " blue
let g:terminal_color_5  = '#C678DD'  | " magenta
let g:terminal_color_6  = '#5699AF'  | " cyan
let g:terminal_color_7  = '#202328'  | " white

"" Bright Colors
let g:terminal_color_8  = '#5B6268'  | " black
let g:terminal_color_9  = '#DA8548'  | " red
let g:terminal_color_10 = '#4DB5BD'  | " green
let g:terminal_color_11 = '#ECBE7B'  | " yellow
let g:terminal_color_12 = '#2257A0'  | " blue (consider #3071db)
let g:terminal_color_13 = '#A9A1E1'  | " magenta
let g:terminal_color_14 = '#46D9FF'  | " cyan
let g:terminal_color_15 = '#DFDFDF'  | " white

"" GUI Font
set guifont=Fira\ Code:h8

"" Function to change the local window current working directory. This is
"" meant to be called from the .zshrc file on a 'cd' hook. This should allow
"" cd'ing in the neovim terminal buffer to cause that window's current working
"" directory to change so that when you ':e' to open a file, it's relative to
"" the terminal's current working directory.
fu Tapi_lcd(buf, cwd) abort
    if has('nvim')
        exe 'lcd '..a:cwd
        return ''
    endif
    let winid = bufwinid(a:buf)
    if winid == -1 || empty(a:cwd)
        return
    endif
    call win_execute(winid, 'lcd '..a:cwd)
endfu

"" }}}
