-- XMonad Configuration File

import XMonad

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

import XMonad.Layout
import XMonad.Layout.Spacing

import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
import XMonad.Util.Ungrab

-- Main
main :: IO ()
main = xmonad $ ewmhFullscreen $ ewmh $ xmobarProp $ myConfig

-- Mod Key
myModMask :: KeyMask
myModMask = mod4Mask  -- Super/Windows Key

-- Border Width
myBorderWidth :: Dimension
myBorderWidth = 1

-- Focussed Border Colour
myFocussedBorderColour :: String
myFocussedBorderColour = "#4e99e4"

-- Default Terminal
myTerminal :: String
myTerminal = "kitty"

-- Default Browser
myBrowser :: String
myBrowser = "brave-browser"

myStartupHook :: X ()
myStartupHook = do
    -- Compositor (not sure what this is for...)
    spawnOnce "picom"
    -- Set background wallpaper
    spawnOnce "feh --bg-fill --no-fehbg ~/.wallpapers/road_1920x1080.jpg"
    -- Enable touchpad tap clicking.
    spawnOnce "xinput --set-prop \"SynPS/2 Synaptics TouchPad\" \"libinput Tapping Enabled\" 1"

-- Keybindings
myKeybindings :: [(String, X ())]
myKeybindings =
    -- XMonad Meta Bindings
    [ ("M-C-r", spawn "xmonad --recompile")    -- Recompile XMonad
    , ("M-S-r", spawn "xmonad --restart"  )    -- Restart XMonad

    -- Applications
    , ("M-b", spawn (myBrowser)           )    -- Open a Browser.
    , ("M-t", spawn (myTerminal)          )    -- Open a Terminal.

    -- System Settings
    , ("M-<Escape>",    -- Lock the Screen.
          spawn "i3lock -n -i /home/jtyler/.wallpapers/sun_rays.png")
    , ("<XF86MonBrightnessUp>",    spawn "lux -a 5%" )    -- Increase Brightness.
    , ("<XF86MonBrightnessDown>",  spawn "lux -s 5%" )    -- Decrease Brightness.
    , ("<XF86AudioRaiseVolume>",   spawn "pactl set-sink-volume @DEFAULT_SINK@ +1.5%")
    , ("<XF86AudioLowerVolume>",   spawn "pactl set-sink-volume @DEFAULT_SINK@ -1.5%")
    , ("<XF86AudioMute>",          spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle" )
    ]

-- Workspaces
myWorkspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

-- Layouts
myLayout = spacing 6 $ Tall 1 (3/100) (1/2)

-- Config
myConfig = def
    { modMask            = myModMask                 -- Rebind Mod to the Super Key
    , terminal           = myTerminal                -- Set the default terminal.
    , borderWidth        = myBorderWidth             -- Set the border width for windows.
    , startupHook        = myStartupHook             -- Set startup hooks.
    , workspaces         = myWorkspaces              -- Set up extra workspaces.
    , layoutHook         = myLayout                  -- Set the layouts.
    , focusedBorderColor = myFocussedBorderColour    -- Focused Border Color.
    }
    `additionalKeysP` myKeybindings
