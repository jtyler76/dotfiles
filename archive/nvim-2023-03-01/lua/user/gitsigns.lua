local status_ok, gitsigns = pcall(require, "gitsigns")
if not status_ok then
    return
end

gitsigns.setup {
    signs = {
        add = { hl = "GitSignsAdd", text = "▎", numhl = "GitSignsAddNr", linehl = "GitSignsAddLn" },
        change = { hl = "GitSignsChange", text = "▎", numhl = "GitSignsChangeNr", linehl = "GitSignsChangeLn" },
        delete = { hl = "GitSignsDelete", text = "契", numhl = "GitSignsDeleteNr", linehl = "GitSignsDeleteLn" },
        topdelete = { hl = "GitSignsDelete", text = "契", numhl = "GitSignsDeleteNr", linehl = "GitSignsDeleteLn" },
        changedelete = { hl = "GitSignsChange", text = "▎", numhl = "GitSignsChangeNr", linehl = "GitSignsChangeLn" },
    },
    signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
    numhl = false, -- Toggle with `:Gitsigns toggle_numhl`
    linehl = false, -- Toggle with `:Gitsigns toggle_linehl`
    word_diff = false, -- Toggle with `:Gitsigns toggle_word_diff`
    watch_gitdir = {
        interval = 1000,
        follow_files = true,
    },
    attach_to_untracked = true,
    current_line_blame = true, -- Toggle with `:Gitsigns toggle_current_line_blame`
    current_line_blame_opts = {
        virt_text = true,
        virt_text_pos = "right_align", -- 'eol' | 'overlay' | 'right_align'
        delay = 250,
        ignore_whitespace = false,
        virt_text_priority = 0,
    },
    current_line_blame_formatter_opts = {
        relative_time = false,
    },
    current_line_blame_formatter = function(name, blame_info, opts)
        if blame_info.author == name then
            blame_info.author = 'You'
        end

        local text
        if blame_info.author == 'Not Committed Yet' then
            text = blame_info.author
        else
            local date_time

            if opts.relative_time then
                date_time = require('gitsigns.util').get_relative_time(tonumber(blame_info['author_time']))
            else
                date_time = os.date('%Y-%m-%d', tonumber(blame_info['author_time']))
            end

            text = string.format('%s | %s | %s ', blame_info.author, date_time, blame_info.summary)
        end

        -- Truncate text if there isn't room
        local ffi = require('ffi')
        ffi.cdef'int curwin_col_off(void);'
        local row = vim.api.nvim_win_get_cursor(0)[1]
        local line = vim.api.nvim_buf_get_lines(0, row-1, row, false)[1]
        line = line:gsub("\t", "    ")  -- need to account for tabs, since they are the width of 4 spaces
        local width = vim.api.nvim_win_get_width(0) - ffi.C.curwin_col_off()
        local available_space = math.max(0, width - #line)

        if available_space == 0 then
            return {{'', 'GitSignsCurrentLineBlame'}}
        elseif #text > available_space then
            text = text:sub(1, available_space-4)
            return {{' '..text, 'GitSignsCurrentLineBlame'}}
        else
            return {{' '..text, 'GitSignsCurrentLineBlame'}}
        end

    end,
    current_line_blame_formatter_nc = function(name, blame_info, opts)

        local ffi = require('ffi')
        ffi.cdef'int curwin_col_off(void);'
        local row = vim.api.nvim_win_get_cursor(0)[1]
        local line = vim.api.nvim_buf_get_lines(0, row-1, row, false)[1]
        line = line:gsub("\t", "    ")  -- need to account for tabs, since they are the width of 4 spaces
        local width = vim.api.nvim_win_get_width(0) - ffi.C.curwin_col_off()
        local available_space = math.max(0, width - #line)

        if available_space < 5 then
            return {{'', 'GitSignsCurrentLineBlame'}}
        elseif available_space < 21 then
            return {{'NYC', 'GitSignsCurrentLineBlame'}}
        else
            return {{'Not yet committed', 'GitSignsCurrentLineBlame'}}
        end

    end,
    sign_priority = 6,
    update_debounce = 100,
    status_formatter = nil, -- Use default
    max_file_length = 40000,
    preview_config = {
        -- Options passed to nvim_open_win
        border = "single",
        style = "minimal",
        relative = "cursor",
        row = 0,
        col = 1,
    },
    yadm = {
        enable = false,
    },
}
