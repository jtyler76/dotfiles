require('project_nvim').setup({
    manual_mode = true,
    detection_methods = { "pattern" },
    patterns = { ".git", ".svn" },
    silent_chdir = false,
})
