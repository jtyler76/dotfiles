vim.g.floaterm_width  = 0.95
vim.g.floaterm_height = 0.95
vim.g.floaterm_title  = "($1/$2)"

-- Set the border to blend in with the colourscheme.
--   To see desired colour for colourscheme: :verbose hi FloatBorder
vim.cmd([[
hi FloatermBorder guifg=#71839b
]])
