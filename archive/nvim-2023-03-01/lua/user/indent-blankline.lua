require('indent_blankline').setup {
    enabled = true,                        -- enable this plugin
    use_treesitter = true,                 -- use treesitter where possible to detemine indentation
    show_first_indent_level = false,       -- don't show line for first level of indentation
    show_current_context_start = false,    -- don't highlight or underline the first line of block
}

