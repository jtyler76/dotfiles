local opts      = { noremap = true, silent = true }
local term_opts = { silent = true }

local keymap = vim.api.nvim_set_keymap

-- Set leader key
keymap('', '<Space>', '<Nop>', opts)
vim.g.mapleader      = ' '
vim.g.maplocalleader = ' '

-- Modes
--     normal mode       = 'n'
--     insert mode       = 'i'
--     visual mode       = 'v'
--     visual block mode = 'x'
--     term mode         = 't'
--     command mode      = 'c'

--- NORMAL MODE MAPPINGS ---

-- Overrides for entering insert mode
keymap('n', 'a', ':nohl<CR>a', opts)
keymap('n', 'A', ':nohl<CR>A', opts)
keymap('n', 'i', ':nohl<CR>i', opts)
keymap('n', 'I', ':nohl<CR>I', opts)
keymap('n', 'o', ':nohl<CR>o', opts)
keymap('n', 'O', ':nohl<CR>O', opts)

-- Overrides for entering normal mode
keymap('n', '<Esc>', ':nohl<CR><Esc>', opts)

-- Overrides for entering visual mode
keymap('n', 'v', ':nohl<CR>v', opts)

-- Overrides for entering visual block mode
keymap('n', '<C-v>', ':nohl<CR><C-v>', opts)

-- Overrides for entering visual line mode
keymap('n', 'V', ':nohl<CR>V', opts)

-- Window Management - Creation
keymap('n', '<leader>wv', ':vsplit<CR>', opts)
keymap('n', '<leader>ws', ':split<CR>',  opts)
keymap('n', '<leader>ww', ':w<CR>',      opts)

-- Window Management - Deletion
keymap('n', '<leader>wq', '<C-w>q', opts)

-- Window Management - Navigation
keymap('n', '<leader>wh', '<C-w>h', opts)
keymap('n', '<leader>wj', '<C-w>j', opts)
keymap('n', '<leader>wk', '<C-w>k', opts)
keymap('n', '<leader>wl', '<C-w>l', opts)

-- Window Management - Resizing
keymap('n', '<C-Up>',    ':resize +2<CR>',          opts)
keymap('n', '<C-Down>',  ':resize -2<CR>',          opts)
keymap('n', '<C-Left>',  ':vertical resize -2<CR>', opts)
keymap('n', '<C-Right>', ':vertical resize +2<CR>', opts)

-- Buffer Management - Switching
keymap('n', '<S-l>', ':bnext<CR>',     opts)
keymap('n', '<S-h>', ':bprevious<CR>', opts)
keymap('n', '<leader>bq', ':bd<CR>', opts)

-- File Management - Tree
keymap('n', '<leader>e', ':NvimTreeToggle<CR>', opts)

-- Text Movement - Move line up or down
keymap('n', '<A-j>', ':m .+1<CR>==', opts)
keymap('n', '<A-k>', ':m .-2<CR>==', opts)

-- Fuzzy Finder
keymap('n', '<leader>sf', ':Telescope find_files<CR>', opts)
keymap('n', '<leader>sg', ':Telescope live_grep<CR>',  opts)
keymap('n', '<leader>sb', ':Telescope buffers<CR>', opts)
keymap('n', '<leader>sp', ':Telescope projects<CR>', opts)
keymap('n', '<leader>sn', ':Telescope notify<CR>', opts)

-- Floating Terminal
keymap('n', '<leader>ft', ':FloatermToggle<CR>', opts)
keymap('n', '<leader>fn', ':FloatermNew<CR>',    opts)
keymap('n', '<leader>fk', ':FloatermKill<CR>',   opts)
keymap('n', '<leader>fh', ':FloatermPrev<CR>',   opts)
keymap('n', '<leader>fl', ':FloatermNext<CR>',   opts)

-- Generate CCLS Data
keymap('n', '<leader>l', ':! compiledb make --always-make --dry-run<CR>', opts)

-- Diagnostics Management
keymap('n', '<leader>dp', ':TroubleToggle<CR>', opts)

-- Zen Mode
keymap('n', '<leader>z', ':ZenMode<CR>', opts)

-- Tab Management
keymap('n', '<leader>tl', ':tabnext<CR>', opts)
keymap('n', '<leader>th', ':tabprevious<CR>', opts)
keymap('n', '<leader>tn', ':tabnew<CR>', opts)
keymap('n', '<leader>tq', ':tabclose<CR>', opts)

-- Clear highlights on ENTER
keymap('n', '<CR>', ':noh<CR>', opts)

-- Gitsigns hunks
keymap('n', '<leader>gn', ':Gitsigns next_hunk<CR><CR>', opts)
keymap('n', '<leader>gp', ':Gitsigns prev_hunk<CR><CR>', opts)


--- INSERT MODE MAPPINGS ---
-- Enter NORMAL mode with jk
keymap('i', 'jk', '<ESC>', opts)


--- VISUAL MODE MAPPINGS ---
-- Persistent Indenting
keymap('v', '<', '<gv', opts)
keymap('v', '>', '>gv', opts)

-- Moving text up and down
keymap("x", "<A-k>", ':m-2<CR>gv=gv',   opts)
keymap('x', '<A-j>', ':m\'>+<CR>gv=gv', opts)

-- Persistent paste (don't replace buffer contents on paste)
keymap('v', 'p', '"_dP', opts)


--- TEMINAL MODE MAPPINGS ---
-- Get to NORMAL mode
keymap('t', '<ESC>', '<C-\\><C-n>', term_opts)
keymap('t', 'jk',    '<C-\\><C-n>', term_opts)

-- Floating Terminal
keymap('t', '<C-h>', '<C-\\><C-n>:FloatermPrev<CR>', term_opts)
keymap('t', '<C-l>', '<C-\\><C-n>:FloatermNext<CR>', term_opts)

