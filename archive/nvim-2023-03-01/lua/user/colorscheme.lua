--local colorscheme = "darkplus"
local colorscheme = 'nightfox'

--[[ local colorscheme = "tokyonight" ]]
--[[ vim.g.tokyonight_colors = { comment = "#888FAC", } ]]

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
    vim.notify("colorscheme " .. colorscheme .. " not found!")
    return
end
