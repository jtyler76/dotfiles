-- resolve issue with CTRL-ALT key codes
vim.g.neovide_input_use_logo = true

-- set mode change cursor animation
vim.g.neovide_cursor_fix_mode = "sonicboom"

-- normal colours
vim.g.terminal_color_0  = '#1C1F24'
vim.g.terminal_color_1  = '#FF6C6B'
vim.g.terminal_color_2  = '#98BE65'
vim.g.terminal_color_3  = '#DA8548'
vim.g.terminal_color_4  = '#51AFEF'
vim.g.terminal_color_5  = '#C678DD'
vim.g.terminal_color_6  = '#5699AF'
vim.g.terminal_color_7  = '#202328'

-- bright colours
vim.g.terminal_color_8  = '#5B6268'
vim.g.terminal_color_9  = '#DA8548'
vim.g.terminal_color_10 = '#4DB5BD'
vim.g.terminal_color_11 = '#ECBE7B'
vim.g.terminal_color_12 = '#2257A0'
vim.g.terminal_color_13 = '#A9A1E1'
vim.g.terminal_color_14 = '#46D9FF'
vim.g.terminal_color_15 = '#DFDFDF'


-- Function to change the local window current working directory. This is
-- meant to be called from the .zshrc file on a 'cd' hook. This should allow
-- cd'ing in the neovim terminal buffer to cause that window's current working
-- directory to change so that when you ':e' to open a file, it's relative to
-- the terminal's current working directory.
vim.cmd [[
fu Tapi_lcd(buf, cwd) abort
    if has('nvim')
        exe 'lcd '..a:cwd
        return ''
    endif
    let winid = bufwinid(a:buf)
    if winid == -1 || empty(a:cwd)
        return
    endif
    call win_execute(winid, 'lcd '..a:cwd)
endfu
]]
