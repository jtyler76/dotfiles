-- lspconfig is used to rig in the various LSP configurations
local lspconfig_rc, lspconfig = pcall(require, 'lspconfig')
if not lspconfig_rc then
    return
end

-- lsp completion?
local cmp_nvim_lsp_rc, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not cmp_nvim_lsp_rc then
    return
end

-- LSP installation management
local lsp_installer_rc, lsp_installer = pcall(require, "nvim-lsp-installer")
if not lsp_installer_rc then
    return
end

-- null-ls
local null_ls_rc, null_ls = pcall(require, 'null-ls')
if not null_ls_rc then
    return
end

lsp_installer.setup()

-- Set up diagnostics
local signs = {
    { name = "DiagnosticSignError", text = "" },
    { name = "DiagnosticSignWarn",  text = "" },
    { name = "DiagnosticSignHint",  text = "" },
    { name = "DiagnosticSignInfo",  text = "" },
}

for _, sign in ipairs(signs) do
    vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
end

vim.diagnostic.config({
    virtual_text = false,
    signs = {
        active = signs,
    },
    update_in_insert = false,
    underline = true,
    severity_sort = true,
    float = {
        focusable = false,
        style = "minimal",
        border = "rounded",
        source = "always",
        header = "",
        prefix = "",
    },
})

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
    vim.lsp.handlers.hover,
    { border = "rounded", }
)

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(
    vim.lsp.handlers.signature_help,
    { border = "rounded", }
)

-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap=true, silent=true }
vim.keymap.set('n', 'gl',        vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d',        vim.diagnostic.goto_prev,  opts)
vim.keymap.set('n', ']d',        vim.diagnostic.goto_next,  opts)
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
    -- Enable completion triggered by <c-x><c-o>
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- Mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local bufopts = { noremap=true, silent=true, buffer=bufnr }
    vim.keymap.set('n', 'gD',        vim.lsp.buf.declaration,     bufopts)
    vim.keymap.set('n', 'gd',        vim.lsp.buf.definition,      bufopts)
    vim.keymap.set('n', 'K',         vim.lsp.buf.hover,           bufopts)
    vim.keymap.set('n', 'gi',        vim.lsp.buf.implementation,  bufopts)
    vim.keymap.set('n', '<C-k>',     vim.lsp.buf.signature_help,  bufopts)
    vim.keymap.set('n', '<space>D',  vim.lsp.buf.type_definition, bufopts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename,          bufopts)
    vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action,     bufopts)
    vim.keymap.set('n', 'gr',        vim.lsp.buf.references,      bufopts)

    -- Document highlighting
    if client.server_capabilities.document_highlight then
        vim.api.nvim_exec(
            [[
                augroup lsp_document_highlight
                    autocmd! * <buffer>
                    autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
                    autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
                augroup END
            ]],
            false
        )
    end
end

local lsp_flags = {
    -- This is the default in Nvim 0.7+
    debounce_text_changes = 150,
}

-- LSP server configurations
local backup_ccls_config = os.getenv('HOME') .. '/.ccls'
lspconfig['ccls'].setup {
    on_attach = on_attach,
    capabilities = cmp_nvim_lsp.default_capabilities(),
    flags     = lsp_flags,
    root_dir = lspconfig.util.root_pattern(
        backup_ccls_config,
        '.ccls',
        '.ccls-cache',
        '.compile_commands.json',
        'compile_commands.json',
        'compile_flags.txt'
    ),
}

-- lspconfig['clangd'].setup {
--     on_attach = on_attach,
--     capabilities = cmp_nvim_lsp.update_capabilities(
--         vim.lsp.protocol.make_client_capabilities()
--     ),
--     flags     = lsp_flags,
-- }

lspconfig['jsonls'].setup {
    on_attach = on_attach,
    capabilities = cmp_nvim_lsp.default_capabilities(),
    flags     = lsp_flags,
}

lspconfig['pyright'].setup {
    on_attach = on_attach,
    capabilities = cmp_nvim_lsp.default_capabilities(),
    flags     = lsp_flags,
}
--[[]]
--[[ lspconfig['sumneko_lua'].setup { ]]
--[[     on_attach = on_attach, ]]
--[[     capabilities = cmp_nvim_lsp.default_capabilities(), ]]
--[[     flags     = lsp_flags, ]]
--[[ } ]]

lspconfig['bashls'].setup {
    on_attach = on_attach,
    capabilities = cmp_nvim_lsp.default_capabilities(),
    flags     = lsp_flags,
}

lspconfig['asm_lsp'].setup {
    on_attach = on_attach,
    capabilities = cmp_nvim_lsp.default_capabilities(),
    flags     = lsp_flags,
    filetypes = { 'nasm', },
}

lspconfig['rust_analyzer'].setup {
    on_attach = on_attach,
    capabilities = cmp_nvim_lsp.default_capabilities(),
    flags     = lsp_flags,
}

require('rust-tools').setup({})

-- Null LS Setup
null_ls.setup {
    debug = false,
    sources = {
        -- Python Diagnostics
        null_ls.builtins.diagnostics.flake8,

        -- C/C++ Diagnostics
        null_ls.builtins.diagnostics.cppcheck,
    },
}


-- Create a toggle for showing diagnostics
vim.g.diagnostics_active = true
function _G.toggle_diagnostics()
    if vim.g.diagnostics_active then
        vim.g.diagnostics_active = false
        vim.diagnostic.disable()
    else
        vim.g.diagnostics_active = true
        vim.diagnostic.enable()
    end
end

vim.api.nvim_set_keymap('n', '<leader>dt', ':call v:lua.toggle_diagnostics()<CR>', {noremap = true, silent = true})
