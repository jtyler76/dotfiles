-- :h dap.txt
local dap = require("dap")

-- Mappings
vim.api.nvim_set_keymap("n", "<leader>dc",  "<Cmd>lua require'dap'.continue()<CR>",                                                    { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>dsn", "<Cmd>lua require'dap'.step_over()<CR>",                                                   { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>dsi", "<Cmd>lua require'dap'.step_into()<CR>",                                                   { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>dso", "<Cmd>lua require'dap'.step_out()<CR>",                                                    { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Leader>db",  "<Cmd>lua require'dap'.toggle_breakpoint()<CR>",                                           { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Leader>dB",  "<Cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>",        { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Leader>dlp", "<Cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Leader>dr",  "<Cmd>lua require'dap'.repl.open()<CR>",                                                   { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Leader>ddl", "<Cmd>lua require'dap'.run_last()<CR>",                                                    { noremap = true, silent = true })

-- C/C++/Rust GDB Adaptor Setup
--   https://github.com/mfussenegger/nvim-dap/wiki/C-C---Rust-(gdb-via--vscode-cpptools)
dap.adapters.cppdbg = {
    id = 'cppdbg',
    type = 'executable',
    command = '/home/jtyler/.nvim_debug_adaptors/vscode-cpptools-1.10.8/extension/debugAdapters/bin/OpenDebugAD7',
}

dap.configurations.cpp = {
    {
        name = "Launch file",
        type = "cppdbg",
        request = "launch",
        program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
        cwd = '${workspaceFolder}',
        stopOnEntry = true,
    },
    {
        name = 'Attach to gdbserver :1234',
        type = 'cppdbg',
        request = 'launch',
        MIMode = 'gdb',
        miDebuggerServerAddress = 'localhost:1234',
        miDebuggerPath = '/usr/bin/gdb',
        cwd = '${workspaceFolder}',
        program = function()
            return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
        end,
    },
}

dap.configurations.c = dap.configurations.cpp
dap.configurations.rust = dap.configurations.cpp
dap.configurations.asm = dap.configurations.cpp
dap.configurations.nasm = dap.configurations.cpp

-- Debug UI
local dapui = require("dapui")
dapui.setup({
    icons = { expanded = "▾", collapsed = "▸" },
    mappings = {
        -- Use a table to apply multiple mappings
        expand = { "<CR>", "<2-LeftMouse>" },
        open = "o",
        remove = "d",
        edit = "e",
        repl = "r",
        toggle = "t",
    },
    -- Expand lines larger than the window
    -- Requires >= 0.7
    expand_lines = vim.fn.has("nvim-0.7"),
    -- Layouts define sections of the screen to place windows.
    -- The position can be "left", "right", "top" or "bottom".
    -- The size specifies the height/width depending on position. It can be an Int
    -- or a Float. Integer specifies height/width directly (i.e. 20 lines/columns) while
    -- Float value specifies percentage (i.e. 0.3 - 30% of available lines/columns)
    -- Elements are the elements shown in the layout (in order).
    -- Layouts are opened in order so that earlier layouts take priority in window sizing.
    layouts = {
        {
            elements = {
            -- Elements can be strings or table with id and size keys.
                { id = "scopes", size = 0.25 },
                "breakpoints",
                "stacks",
                "watches",
            },
            size = 40, -- 40 columns
            position = "left",
        },
        {
            elements = {
                "repl",
                "console",
            },
            size = 0.25, -- 25% of total lines
            position = "bottom",
        },
    },
    floating = {
        max_height = nil, -- These can be integers or a float between 0 and 1.
        max_width = nil, -- Floats will be treated as percentage of your screen.
        border = "single", -- Border style. Can be "single", "double" or "rounded"
        mappings = {
            close = { "q", "<Esc>" },
        },
    },
    windows = { indent = 1 },
    render = {
        max_type_length = nil, -- Can be integer or nil.
    }
})

dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close()
end

vim.fn.sign_define('DapBreakpoint', {text='ﱣ', texthl='Error',      linehl='', numhl=''})
vim.fn.sign_define('DapStopped',    {text='ﱣ', texthl='Identifier', linehl='', numhl=''})
