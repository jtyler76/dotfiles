local notify = require("notify")

notify.setup({
    level = vim.log.levels.TRACE,
})

vim.notify = notify

