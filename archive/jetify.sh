#!/bin/bash

TXT_BOLD=$(tput bold)
TXT_RESET=$(tput sgr0)

LOG="/tmp/jetify.log"

## list of tools to install with apt
TOOLS="vim tree meld fonts-ibm-plex zsh ripgrep fd-find"

function echo_bold() {
    echo "${TXT_BOLD}${1}${TXT_RESET}"
}

function install_tools() {
    echo_bold "installing helpful tools..."
    sudo apt-get install -y ${TOOLS}
}

function setup_terminator() {
    echo_bold "installing and configuring terminator..."

    ## add latest apt repository
    sudo add-apt-repository ppa:mattrose/terminator
    sudo apt-get update

    ## install terminator
    sudo apt install terminator

    ## set custom configuration
    cp .config/terminator/config ~/.config/terminator/config
}

function setup_emacs() {
    echo_bold "installing and configuring emacs..."

    ## add latest apt repository
    sudo add-apt-repository ppa:kelleyk/emacs
    sudo apt-get update

    ## install emacs
    sudo apt-get install emacs27
}

function configure_zsh() {
    echo_bold "installing oh-my-zsh..."

    ## remove existing oh-my-zsh install
    mv ~/.oh-my-zsh ~/.backup_oh-my-zsh

    ## install oh-my-zsh
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

    ## set custom configuration
    cp -r .oh-my-zsh/* ~/.oh-my-zsh/.

    ## set custom .zshrc
    cp .zshrc ~/.zshrc

    ## change zsh to the default shell
    chsh -s $(which zsh)
}

function setup_doom() {
    echo_bold "installing and configuring doom..."

    ## backup existing emacs configs
    mv ~/.emacs.d ~/.emacs.d.backup
    mv ~/.emacs ~/.emacs.backup

    ## install doom
    git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
    ~/.emacs.d/bin/doom install

    ## copy over custom config
    cp -r .doom.d/* ~/.doom.d/.

    ## synchronize doom
    ~/.emacs.d/bin/doom sync
}

function main() {

    ## clear log
    echo -n "" > ${LOG}
    echo "* logging to ${LOG}"

    ## install tools
    install_tools

    ## install/setup terminator
    setup_terminator

    ## install/setup zsh shell
    configure_zsh

    ## install emacs
    setup_emacs

    ## install/setup doom
    setup_doom
}

main $@
