# Dotfiles
This repository contains README's, configurations and scripts to detail and help set up my preferred Linux system configurations.

# Operating System
## Linux Distribution
My preferred Linux Distribution is [Arch](https://archlinux.org/).

## Window Manager
I use two window managers; [KDE Plasma](https://kde.org/plasma-desktop/) and [i3wm](https://i3wm.org/).  

I like using a tiling window manager like i3wm most of the time, but I also like have a solid backup that can do whatever I need in a pinch, and KDE Plasma is that.

# Terminal Emulator
My terminal emulator of choice is [Kitty](https://sw.kovidgoyal.net/kitty/). It's GPU based, very fast, and supports all the nonsense I like to have. It's also very easy to configure.

# Shell
My shell of choice is [zsh](https://zsh.sourceforge.io/). And I manage my **zsh** configuration through [oh-my-zsh](https://ohmyz.sh/).

In zsh/oh-my-zsh, shell prompts are managed via themes. There are several provided themes located in `~/.oh-my-zsh/themes`, but I've written my own based off of some of these called `jetyler.zsh-theme`. If you'd like to use this one, you can copy it into your themes folder.  

Themes are set in your `~/.zshrc` file. Simply set `ZSH_THEME="jetyler"` to set the theme.

# Editor
My editor of choice is [Neovim](https://neovim.io/). I like Emacs as well, but after having tried both I feel most comfortable in Vim.  

I sometimes use [Neovide](https://neovide.dev/) as a nice graphical front end for Neovim.
